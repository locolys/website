# Generated by Django 3.2.13 on 2022-05-04 14:42

import datetime
from django.db import migrations, models
import django.db.models.deletion
import locolys.models.pages
import wagtail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0066_collection_management_permissions'),
        ('locolys', '0004_newslettersubscriber'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostIndexPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('navigation_menu', models.CharField(choices=[('main', 'Principal'), ('footer', 'Pied de page'), ('all', 'Tous')], default='all', max_length=10, verbose_name='Menu de navigation')),
            ],
            options={
                'verbose_name': "page d'index des articles",
                'verbose_name_plural': "pages d'index des articles",
            },
            bases=(locolys.models.pages.PageContextMixin, 'wagtailcore.page'),
        ),
        migrations.CreateModel(
            name='PostPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('body', wagtail.fields.RichTextField(verbose_name='contenu')),
                ('date_published', models.DateField(default=datetime.date.today, verbose_name='date de parution')),
            ],
            options={
                'verbose_name': 'article',
                'verbose_name_plural': 'articles',
            },
            bases=(locolys.models.pages.PageContextMixin, 'wagtailcore.page'),
        ),
    ]
