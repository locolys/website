from django import template

from wagtail.models import Site

from locolys.models.choices import NavigationMenu

register = template.Library()


@register.simple_tag(takes_context=True)
def get_site_root(context):
    """Retourne la page racine du site déterminé par la requête."""
    return Site.find_for_request(context["request"]).root_page


def is_page_active(page, current_page=None):
    """
    Détermine si la page `page` est considérée active en fonction de la page
    actuelle `current_page`, c'est-à-dire si c'est la même ou si c'est une
    sous-page de celle-ci.
    """
    return (
        current_page.url_path.startswith(page.url_path)
        if current_page
        else False
    )


def get_page_children(parent, menu):
    """
    Retourne les pages enfants de `page` qui sont visibles et à faire
    apparaître dans le menu `menu` ou dans tous.
    """
    return [
        page
        for page in (
            parent.get_children().live().in_menu().specific(defer=True)
        )
        if (
            page.navigation_menu == NavigationMenu.ALL
            or page.navigation_menu == menu
        )
    ]


@register.inclusion_tag("tags/main_menu.html", takes_context=True)
def main_menu(context, parent, current_page=None):
    """
    Retourne les élements du menu principal présenté sur deux niveaux, avec,
    pour chaque page, si celle-ci est active - en fonction de la page actuelle
    `current_page` - et son sous-menu - si elle contient des pages enfants à
    faire apparaître dans le menu.
    """
    menu_items = get_page_children(parent, NavigationMenu.MAIN)
    for page in menu_items:
        page.is_active = is_page_active(page, current_page)
        # Récupère les pages enfants qui sont à faire apparaître les menus
        # sans filtrer sur le menu de navigation, inutile dans notre cas
        page.children = page.get_children().live().in_menu()
        page.has_children = len(page.children) > 0
        for child_page in page.children:
            child_page.is_active = is_page_active(child_page, current_page)
    return {
        "items": menu_items,
        "request": context["request"],
    }


@register.inclusion_tag("tags/footer_menu.html", takes_context=True)
def footer_menu(context, parent, current_page=None):
    """
    Retourne les éléments du menu affiché en pied de page sur un seul niveau,
    avec, pour chaque page, si celle-ci est active - en fonction de la page
    actuelle `current_page`.
    """
    menu_items = get_page_children(parent, NavigationMenu.FOOTER)
    for page in menu_items:
        page.is_active = is_page_active(page, current_page)
    return {
        "items": menu_items,
        "request": context["request"],
    }
