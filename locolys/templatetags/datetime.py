import datetime

from django import template

register = template.Library()


@register.filter
def datesince(value):
    today = datetime.date.today()
    if value == today:
        return "Aujourd’hui"
    if today - value == datetime.timedelta(days=1):
        return "Hier"
    return value
