from django import template

register = template.Library()


@register.inclusion_tag("tags/alert.html", takes_context=False)
def alert(level, message, **kwargs):
    kwargs["level"] = level
    kwargs["message"] = message
    return kwargs


@register.inclusion_tag("tags/icon.html", takes_context=False)
def icon(name, **kwargs):
    kwargs["name"] = name
    return kwargs
