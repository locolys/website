import datetime

from django.db import models

from wagtail.fields import RichTextField, StreamField
from wagtail.models import Page

from locolys.blocks import BodyBlock, HomeButtonBlock

from .choices import NavigationMenu


class PageContextMixin:
    """
    Définis le contexte nécessaire pour le rendu d'une page.
    """

    def get_title(self):
        return self.title

    def get_seo_title(self):
        return self.seo_title or self.title

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["title"] = self.get_title()
        context["seo_title"] = self.get_seo_title()
        return context


class AbstractBasePage(PageContextMixin, Page):
    """
    Classe de base abstraite pour les pages du site.
    """

    class Meta:
        abstract = True

    # Champs

    navigation_menu = models.CharField(
        max_length=10,
        choices=NavigationMenu.choices,
        default=NavigationMenu.ALL,
        verbose_name="Menu de navigation",
    )


class HomePage(AbstractBasePage):
    """
    La page d'accueil du site.
    """

    class Meta:
        verbose_name = "page d'accueil"
        verbose_name_plural = "pages d'accueil"

    # Champs

    header_background_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="image de fond",
        help_text=(
            "Choisissez une image d'une largeur minimale de 1920px afin "
            "d'avoir un rendu convenable sur un grand écran."
        ),
    )
    header_content = StreamField(
        [("button", HomeButtonBlock())],
        collapsed=True,
        use_json_field=True,
        blank=True,
        verbose_name="contenu",
    )

    last_posts_title = models.CharField(
        max_length=100,
        default="Les dernières actualités",
        verbose_name="titre",
    )
    last_posts_limit = models.PositiveSmallIntegerField(
        choices=[(i, i) for i in range(4)],
        default=0,
        verbose_name="nombre d'articles",
    )

    presentation = RichTextField(
        features=["h3", "h4", "bold", "italic", "link"],
        verbose_name="texte de présentation",
    )

    block_1_background_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="image de fond",
    )
    block_1_content = StreamField(
        [("button", HomeButtonBlock())],
        max_num=1,
        collapsed=True,
        use_json_field=True,
        blank=True,
        verbose_name="contenu",
    )

    block_2_background_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        verbose_name="image de fond",
    )
    block_2_content = StreamField(
        [("button", HomeButtonBlock())],
        max_num=1,
        collapsed=True,
        use_json_field=True,
        blank=True,
        verbose_name="contenu",
    )

    # Configuration de la page

    parent_page_types = ["wagtailcore.Page"]

    template = "pages/home_page.html"

    # Méthodes

    def get_context(self, request):
        context = super().get_context(request)
        if self.last_posts_limit:
            context["last_posts"] = list(
                PostPage.objects.descendant_of(self)
                .live()
                .order_by("-date_published")[: self.last_posts_limit]
            )
        return context


class ContentPage(AbstractBasePage):
    """
    Une page de contenu générique.
    """

    class Meta:
        verbose_name = "page de contenu"
        verbose_name_plural = "pages de contenu"

    # Champs

    introduction = models.TextField(
        max_length=255,
        blank=True,
        verbose_name="chapeau",
        help_text="Un texte bref introduisant la page.",
    )
    show_header = models.BooleanField(
        default=True,
        verbose_name="afficher l'en-tête",
    )

    body = StreamField(BodyBlock(), use_json_field=True, verbose_name="contenu")

    # Configuration de la page

    show_in_menus_default = True

    template = "pages/content_page.html"


class PostPage(PageContextMixin, Page):
    """
    Un article daté avec un contenu simple.
    """

    class Meta:
        verbose_name = "article"
        verbose_name_plural = "articles"

    # Champs

    body = RichTextField(
        features=[
            "h3",
            "h4",
            "bold",
            "italic",
            "ol",
            "ul",
            "link",
            "document-link",
            "image",
        ],
        verbose_name="contenu",
    )

    date_published = models.DateField(
        default=datetime.date.today,
        verbose_name="date de parution",
    )

    # Configuration de la page

    parent_page_types = ["PostIndexPage"]
    subpage_types = []

    template = "pages/post_page.html"


class PostIndexPage(AbstractBasePage):
    """
    La page du site listant les articles.
    """

    class Meta:
        verbose_name = "page d'index des articles"
        verbose_name_plural = "pages d'index des articles"

    # Configuration de la page

    max_count_per_parent = 1

    subpage_types = ["PostPage"]

    template = "pages/post_index_page.html"

    # Méthodes

    def get_context(self, request):
        context = super().get_context(request)
        context["posts"] = (
            PostPage.objects.descendant_of(self)
            .live()
            .order_by("-date_published")
        )
        return context
