from django.db import models

from wagtail.contrib.settings.models import BaseSiteSetting, register_setting

from .newsletter import Newsletter, NewsletterSubscription  # noqa: F401
from .pages import ContentPage, HomePage  # noqa: F401
from .users import User  # noqa: F401


@register_setting
class SiteSettings(BaseSiteSetting):
    """
    Paramètres du pied de page du site.
    """

    class Meta:
        verbose_name = "paramètres du site"
        verbose_name_plural = "paramètres des sites"

    # Champs

    facebook_url = models.URLField("Facebook", blank=True)
    instagram_url = models.URLField("Instagram", blank=True)
