from django.db import models


class NavigationMenu(models.TextChoices):
    MAIN = "main", "Principal"
    FOOTER = "footer", "Pied de page"
    ALL = "all", "Tous"
