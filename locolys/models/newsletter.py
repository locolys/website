from django.conf import settings
from django.db import models
from django.utils import timezone

from wagtail.models import Orderable

from ..utils import normalize_email


class Newsletter(Orderable):
    """
    Une infolettre à laquelle il est possible de s'abonner.
    """

    class Meta:
        ordering = ["sort_order"]
        verbose_name = "infolettre"
        verbose_name_plural = "infolettres"

    # Champs

    name = models.CharField(max_length=100, verbose_name="nom")
    description = models.TextField(verbose_name="description")

    # Méthodes

    def __str__(self):
        return self.name


class NewsletterSubscription(models.Model):
    """
    Un abonnement à une ou plusieurs infolettres, pouvant être lié à un
    compte utilisateur.
    """

    class Meta:
        verbose_name = "abonnement aux infolettres"
        verbose_name_plural = "abonnements aux infolettres"

    # Champs

    is_active = models.BooleanField(default=True, verbose_name="actif")

    email = models.EmailField(
        unique=True,
        error_messages={
            "unique": "Cette adresse mail est déjà abonnée aux infolettres."
        },
        verbose_name="adresse mail",
    )
    newsletters = models.ManyToManyField(
        Newsletter,
        related_name="subscriptions",
        related_query_name="subscription",
        verbose_name="infolettres",
    )

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="newsletter_subscription",
        blank=True,
        null=True,
    )

    date_subscribed = models.DateTimeField(
        default=timezone.now,
        editable=False,
        verbose_name="date d’abonnement",
    )

    # Méthodes

    def clean(self):
        super().clean()

        self.email = normalize_email(self.email)
