from django import forms

from wagtail.admin.panels import FieldPanel, FieldRowPanel, MultiFieldPanel

from locolys.admin.widgets import NumericInput


def set_models_panels():
    from .models import Product

    Product.panels = [
        FieldPanel(
            "name",
            classname="title",
            widget=forms.TextInput(
                attrs={"placeholder": "Nom du produit"},
            ),
        ),
        MultiFieldPanel(
            [
                FieldPanel("category"),
                FieldRowPanel(
                    [
                        FieldPanel("unit_type"),
                        FieldPanel(
                            "variant_unit_name",
                            help_text=(
                                "Dans le cas où l'unité de mesure est à la "
                                "pièce."
                            ),
                        ),
                    ]
                ),
            ],
            heading="Détails",
        ),
        MultiFieldPanel(
            [
                FieldPanel("display_name"),
                FieldPanel("display_as"),
                FieldRowPanel(
                    [
                        FieldPanel("units", widget=NumericInput(format=".3g")),
                        FieldPanel("price", widget=NumericInput(format=".2f")),
                    ]
                ),
                FieldPanel("on_demand"),
            ],
            heading="Variante",
        ),
    ]
