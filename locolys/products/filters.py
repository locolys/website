from django.db.models import Sum

from wagtail.admin.filters import WagtailFilterSet

import django_filters

from locolys.admin.filters import IntegerRangeWidget
from locolys.models import User

from .models import Product, Stock


class ProductFilterSet(WagtailFilterSet):
    class Meta:
        model = Product
        fields = ["category", "amount"]

    # Champs

    category = django_filters.ChoiceFilter(choices=Product.CATEGORY_CHOICES)
    amount = django_filters.RangeFilter(
        label="Quantité", widget=IntegerRangeWidget
    )

    # Méthodes

    def filter_queryset(self, queryset):
        # Annote chaque produit avec sa quantité en stock utilisée comme filtre
        queryset = queryset.annotate(amount=Sum("stock__amount", default=0))
        return super().filter_queryset(queryset)


class StockFilterSet(WagtailFilterSet):
    class Meta:
        model = Stock
        fields = ["user", "amount"]

    # Champs

    user = django_filters.ModelChoiceFilter(
        queryset=User.objects.filter(product_stock__isnull=False).distinct(),
        label="Producteur⋅rice",
        required=True,
        empty_label=None,
    )
    amount = django_filters.RangeFilter(
        label="Quantité", widget=IntegerRangeWidget
    )

    # Méthodes

    def __init__(self, data=None, queryset=None, *, request=None, **kwargs):
        if data is not None and request is not None and not data.get("user"):
            data = data.copy()
            data["user"] = request.user.id

        super().__init__(data, queryset=queryset, request=request, **kwargs)
