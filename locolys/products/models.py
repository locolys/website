import re
from decimal import Decimal
from math import ceil

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from django.db import models
from django.db.models import Sum

from wagtail.search import index

from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel

from .utils import format_amount, pluralize


class Product(index.Indexed, ClusterableModel):
    class Meta:
        ordering = ["name", "display_name"]
        verbose_name = "produit"
        verbose_name_plural = "produits"

    CATEGORY_CHOICES = [
        ("AUTRES", "Autres"),
        ("BIERE", "Bière"),
        ("BOISSONS, JUS", "Boissons, jus"),
        ("CONSERVES & BOCAUX", "Conserves & bocaux"),
        ("FRUITS", "Fruits"),
        ("HERBES & ÉPICES", "Herbes & épices"),
        ("HUILES & VINAIGRES", "Huiles & vinaigres"),
        ("LÉGUMES", "Légumes"),
        ("ŒUFS", "Œufs"),
        ("PAINS, FARINES, CÉRÉALES", "Pains, farines, céréales"),
        ("THÉS, CAFÉS", "Thés, cafés"),
    ]

    UNIT_TYPE_CHOICES = [
        ("g", "Poids (g)"),
        ("kg", "Poids (kg)"),
        ("T", "Poids (T)"),
        ("mL", "Volume (mL)"),
        ("L", "Volume (L)"),
        ("kL", "Volume (kL)"),
        ("", "À la pièce"),
    ]

    search_fields = [
        index.AutocompleteField("name"),
        index.AutocompleteField("variant_display"),
        index.SearchField("name", boost=10),
        index.SearchField("variant_display"),
    ]

    # Champs

    name = models.CharField(max_length=150, verbose_name="nom")
    category = models.CharField(
        max_length=100,
        choices=CATEGORY_CHOICES,
        verbose_name="catégorie",
    )
    unit_type = models.CharField(
        max_length=10,
        blank=True,
        choices=UNIT_TYPE_CHOICES,
        verbose_name="unité de mesure",
    )
    variant_unit_name = models.CharField(
        max_length=50,
        blank=True,
        verbose_name="nom de la pièce",
    )

    display_name = models.CharField(
        max_length=150,
        blank=True,
        verbose_name="nom affiché",
    )
    display_as = models.CharField(
        max_length=150,
        blank=True,
        verbose_name="unité affichée",
    )
    units = models.DecimalField(
        max_digits=6,
        decimal_places=3,
        verbose_name="poids, volume ou quantité",
    )
    price = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        verbose_name="prix",
        help_text="Le prix du produit TTC sans les marges",
    )
    on_demand = models.BooleanField(verbose_name="stock infini")

    variant_display = models.CharField(max_length=150, editable=False)

    # Méthodes

    def __str__(self):
        return "{} / {}".format(
            self.name,
            self.variant_display or self.get_variant_display(),
        )

    def clean(self):
        if not self.unit_type and not self.variant_unit_name:
            raise ValidationError(
                {
                    "variant_unit_name": (
                        "Ce champ est requis pour une unité de mesure à la "
                        "pièce."
                    )
                }
            )

    def save(self, *args, **kwargs):
        self.variant_display = self.get_variant_display()
        super().save(*args, **kwargs)

    def get_variant_display(self):
        """Retourne le nom de la variante du produit."""
        if self.display_name:
            return "{} ({})".format(
                self.display_name,
                self.get_unit_display(),
            )
        return self.get_unit_display()

    def get_unit_display(self):
        """Retourne l'unité du produit à afficher."""
        if self.display_as:
            return self.display_as
        if self.unit_type:
            return format_amount(float(self.units), self.unit_type)
        return "{0:.3g} {1}".format(
            float(self.units),
            pluralize(self.variant_unit_name, self.units),
        )


class StockManager(models.Manager):
    def divide_between_producers(self, product, amount):
        stocks = self.filter(product=product, amount__gt=0).order_by(
            "-part_percentage"
        )

        if not stocks:
            raise ValueError(
                "Aucun stock disponible pour le produit « {} »".format(product)
            )

        def process_stock(stock, needed):
            available = stock.amount

            if needed > available:
                needed = available

            stock.supplied += needed
            stock.amount = available - needed

            return needed

        if len(stocks) == 1:
            supplied = process_stock(stocks[0], amount)

            if supplied < amount:
                raise ValueError(
                    "Stocks insuffisants pour le produit « {} » "
                    "({} manquant(s))".format(product, amount - supplied)
                )

            return [stocks[0]]

        missing = amount
        total_parts = 0

        # On commence par tenir compte de la quantité disponible et de la part
        # de chaque stock pour tenter d'arriver à la quantité demandée
        for stock in stocks:
            stock_part = stock.part_percentage
            needed = ceil(missing * (stock_part + total_parts) / 100)

            missing -= process_stock(stock, needed)
            total_parts += stock_part

            if missing == 0:
                break

        if missing > 0:
            available_stocks = [s for s in stocks if s.amount]
            available_amount = sum(s.amount for s in available_stocks)

            if available_amount < missing:
                raise ValueError(
                    "Stocks insuffisants pour le produit « {} » "
                    "({} manquant(s))".format(
                        product, missing - available_amount
                    )
                )

            total_parts = 0
            part = int(100 / len(available_stocks))

            # On complète avec les stocks encore disponibles en répartissant
            # équitablement la quantité manquante
            for stock in available_stocks:
                needed = ceil(missing * (part + total_parts) / 100)

                missing -= process_stock(stock, needed)

            assert missing == 0

        return [stock for stock in stocks if stock.supplied]


class StockQuerySet(models.QuerySet):
    def total_amount(self):
        return self.aggregate(
            total=Sum("amount", default=Decimal("0")),
        )["total"]

    def total_part_percentage(self):
        return self.aggregate(
            total=Sum("part_percentage", default=Decimal("0")),
        )["total"]


class Stock(index.Indexed, models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["product", "user"],
                name="unique_user_product_stock",
            ),
        ]
        verbose_name = "stock"
        verbose_name_plural = "stocks"

    objects = StockManager.from_queryset(StockQuerySet)()

    search_fields = [
        index.FilterField("product"),
        index.FilterField("user"),
        index.RelatedFields(
            "product",
            [
                index.AutocompleteField("name"),
                index.AutocompleteField("variant_display"),
                index.SearchField("name", boost=10),
                index.SearchField("variant_display"),
            ],
        ),
    ]

    # Champs

    product = ParentalKey(
        Product,
        on_delete=models.CASCADE,
        related_name="stocks",
        related_query_name="stock",
        verbose_name="produit",
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="product_stocks",
        related_query_name="product_stock",
        verbose_name="producteur⋅rice",
    )

    amount = models.PositiveSmallIntegerField(
        default=0,
        verbose_name="quantité",
    )
    part_percentage = models.PositiveSmallIntegerField(
        default=100,
        help_text=(
            "Le pourcentage des commandes qui peut être fourni par ce stock, "
            "dans le cas où plusieurs producteur⋅rices ont ce produit."
        ),
        validators=[MaxValueValidator(100)],
        verbose_name="part",
    )

    updated_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name="dernière mise à jour",
    )

    # Méthodes

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super().from_db(db, field_names, values)
        instance.supplied = 0
        return instance

    def __str__(self):
        return "{} ({})".format(self.product, self.user.get_short_name())

    def clean(self):
        if not hasattr(self, "product"):
            return

        queryset = (
            self.product.stocks.all()
            if self.pk is None
            else self.product.stocks.exclude(pk=self.pk)
        )
        total_part_percentage = (
            queryset.total_part_percentage() + self.part_percentage
        )
        if total_part_percentage > 100:
            raise ValidationError(
                {
                    "part_percentage": (
                        "La somme des parts pour ce produit est supérieure à "
                        "100 ({}).".format(total_part_percentage)
                    )
                }
            )

    def unique_error_message(self, model_class, unique_check):
        if unique_check == ("product", "user"):
            return ValidationError(
                message=(
                    "Un stock de ce produit existe déjà pour ce "
                    "producteur⋅rice."
                ),
                code="unique_together",
            )
        return super().unique_error_message(model_class, unique_check)


class ProductOrder:
    def __init__(
        self,
        product: Product = None,
        unit_price: Decimal = Decimal(0),
        total_amount: int = 0,
    ):
        self.product = product
        self.unit_price = unit_price
        self.total_amount = total_amount

        self.orders = {}
        self.stocks = []

    def __repr__(self):
        return (
            'ProductOrder(product="{}", unit_price={}, total_amount={})'.format(
                self.product,
                self.unit_price,
                self.total_amount,
            )
        )

    def add_order(self, order_cycle, amount):
        self.orders[order_cycle] = self.orders.get(order_cycle, 0) + amount
        self.total_amount += amount


class OrderCycle:
    STRING_RE = re.compile(r"(.+) ([0-9]{2}/[0-9]{2}/[0-9]{4})")

    def __init__(self, code: str, place: str, date: str):
        self.code = code
        self.place = place
        self.date = date

    def __repr__(self):
        return 'OrderCycle(code="{}", place={}, date={})'.format(
            self.code,
            self.place,
            self.date,
        )

    @classmethod
    def from_string(cls, string):
        m = cls.STRING_RE.match(string)

        if not m:
            raise ValueError(
                f"Le cycle de vente {string} n'est pas bien formaté"
            )

        place, date = m.group(1, 2)
        code = place[:3].upper()

        return cls(code, place, date)
