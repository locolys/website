import csv
import datetime
from collections import OrderedDict

from django.contrib.admin.utils import quote
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db.models import Sum
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.urls import path, reverse
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.views.generic import FormView

from wagtail.admin import messages
from wagtail.admin.panels import FieldPanel
from wagtail.admin.ui.tables import Column, Table, TitleColumn
from wagtail.admin.views.generic import (
    PermissionCheckedMixin,
    WagtailAdminTemplateMixin,
)
from wagtail.admin.viewsets.base import ViewSet

from weasyprint import HTML

from locolys.admin.ui import ActionColumn, FieldColumn, FormSetTable
from locolys.admin.views import generic
from locolys.models import User

from ..filters import ProductFilterSet, StockFilterSet
from ..forms import OrdersImportForm, StockUpdateForm, UserStockCreateForm
from ..models import Product, Stock
from ..permissions import product_permission_policy, stock_permission_policy


class NumberInputColumn(Column):
    cell_template_name = "products/admin/tables/number_input_cell.html"


class ShopBreadcrumbsMixin:
    breadcrumbs_items = [{"label": "Boutique"}]
    breadcrumbs_is_expanded = True


# PRODUCT
# ------------------------------------------------------------------------------


class ProductIndexView(ShopBreadcrumbsMixin, generic.IndexView):
    any_permission_required = ["view", "add", "change", "delete"]
    default_ordering = "name"
    filterset_class = ProductFilterSet
    use_autocomplete = True
    page_title = "Produits"
    add_item_label = "Ajouter un produit"

    columns = [
        TitleColumn(
            "__str__",
            label="Produit",
            url_name="shopadmin_product:inspect",
            sort_key="name",
        ),
        Column("price", label="Prix", sort_key="price"),
        Column("amount", label="Quantité", sort_key="amount"),
    ]


class ProductCreateView(ShopBreadcrumbsMixin, generic.CreateView):
    page_title = "Ajouter un produit"
    success_message = "Produit « %(object)s » ajouté."
    error_message = "Le produit ne peut être créé du fait d'erreurs."


class ProductEditView(ShopBreadcrumbsMixin, generic.EditView):
    success_message = "Produit « %(object)s » mis à jour."
    error_message = "Le produit ne peut être enregistré du fait d'erreurs."


class ProductDeleteView(ShopBreadcrumbsMixin, generic.DeleteView):
    confirmation_message = "Êtes-vous sûr⋅e de vouloir supprimer ce produit ?"
    success_message = "Produit « %(object)s » supprimé."


class ProductInspectView(ShopBreadcrumbsMixin, generic.InspectView):
    template_name = "products/admin/product/inspect.html"

    fields = [
        "name",
        "category",
        ("unit_type", "variant_unit_name"),
        ("display_name", "display_as"),
        ("units", "price"),
    ]

    @cached_property
    def can_edit_stocks(self):
        return stock_permission_policy.user_has_permission(
            self.request.user, "change"
        )

    def get(self, request, *args, **kwargs):
        self.stock_formset = (
            self.get_stock_formset() if self.can_edit_stocks else None
        )

        context = self.get_context_data()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if not self.can_edit_stocks:
            raise PermissionDenied

        self.stock_formset = self.get_stock_formset()

        if self.stock_formset.is_valid():
            self.stock_formset.save()

            messages.success(
                request,
                "La quantité de {} stock(s) a été mise à jour.".format(
                    len(self.stock_formset.changed_objects),
                ),
            )

            return HttpResponseRedirect(self.get_success_url())

        context = self.get_context_data()
        return self.render_to_response(context)

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related("stocks")
            .annotate(amount=Sum("stock__amount", default=0))
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["can_edit_stocks"] = self.can_edit_stocks
        context["stocks_table"] = self.get_stock_table()
        return context

    def get_stock_table(self):
        if self.stock_formset:
            columns = [
                TitleColumn(
                    "user",
                    url_name="shopadmin_stock:edit",
                    label="Producteur⋅rice",
                ),
                FieldColumn("part_percentage", label="Part (en %)", width=200),
                FieldColumn("amount", label="Quantité", width=200),
            ]
            return FormSetTable(columns, self.stock_formset)

        columns = [
            TitleColumn("user", label="Producteur⋅rice"),
            Column("part_percentage", label="Part (en %)", width=200),
            Column("amount", label="Quantité", width=200),
        ]
        return Table(columns, self.object.stocks.all())

    def get_stock_formset(self):
        formset_class = modelformset_factory(
            Stock,
            form=StockUpdateForm,
            fields=["amount", "part_percentage"],
            extra=0,
            edit_only=True,
        )
        kwargs = {"queryset": self.object.stocks.all()}

        if self.request.method in ("POST", "PUT"):
            kwargs.update(
                {
                    "data": self.request.POST,
                    "files": self.request.FILES,
                }
            )
        return formset_class(**kwargs)

    def get_success_url(self):
        return reverse(
            "shopadmin_product:inspect", args=(quote(self.object.pk),)
        )


class ProductViewSet(generic.ViewSet):
    icon = "carrot"
    model = Product
    permission_policy = product_permission_policy

    index_view_class = ProductIndexView
    add_view_class = ProductCreateView
    edit_view_class = ProductEditView
    delete_view_class = ProductDeleteView
    inspect_view_class = ProductInspectView
    inspect_view_enabled = True


# STOCK
# ------------------------------------------------------------------------------


class StockIndexView(ShopBreadcrumbsMixin, generic.IndexView):
    ordering = ["amount", "-part_percentage"]
    default_ordering = "product__name"
    filterset_class = StockFilterSet
    search_fields = ["product__name"]
    page_title = "Stocks"
    add_item_label = "Ajouter un stock"

    def get_product_inspect_url(instance):
        return reverse(
            "shopadmin_product:inspect", args=(quote(instance.product_id),)
        )

    columns = [
        TitleColumn(
            "product",
            label="Produit",
            get_url=get_product_inspect_url,
            sort_key="product__name",
        ),
        Column("part_percentage", label="Part (en %)", width=150),
        FieldColumn("amount", label="Quantité", sort_key="amount", width=200),
        ActionColumn(
            "delete",
            title="Supprimer",
            url_name="shopadmin_stock:delete",
            icon_name="bin",
        ),
    ]
    table_class = FormSetTable

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        self.formset = self.get_formset(self.object_list)

        context = self.get_context_data()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        self.formset = self.get_formset(self.object_list)

        if self.formset.is_valid():
            self.formset.save()

            messages.success(
                request,
                "La quantité de {} stock(s) a été mise à jour.".format(
                    len(self.formset.changed_objects),
                ),
            )

        context = self.get_context_data()
        return self.render_to_response(context)

    def get_base_queryset(self):
        return super().get_base_queryset().select_related("product")

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.search_query:
            filters = {
                field + "__icontains": self.search_query
                for field in self.search_fields
            }
            queryset = queryset.filter(**filters)

        return queryset

    def search_queryset(self, queryset):
        # La queryset est filtrée dès le début dans `self.get_queryset()` afin
        # que la recherche soit prise en compte dans le formset également
        return queryset

    def get_table(self, object_list, **kwargs):
        return self.table_class(
            self.columns,
            self.formset,
            ordering=self.get_ordering(),
            **kwargs,
        )

    def get_formset(self, queryset):
        formset_class = modelformset_factory(
            Stock,
            form=StockUpdateForm,
            fields=["amount"],
            extra=0,
            edit_only=True,
        )
        kwargs = {"queryset": queryset}

        if self.request.method in ("POST", "PUT"):
            kwargs.update(
                {
                    "data": self.request.POST,
                    "files": self.request.FILES,
                }
            )
        return formset_class(**kwargs)

    def get_add_url(self):
        add_url = super().get_add_url()
        # Garde le même producteur⋅rice pour l'ajout d'un stock
        if self.filters and self.filters.data.get("user"):
            add_url += "?user=%d" % int(self.filters.data["user"])
        return add_url


class StockCreateView(ShopBreadcrumbsMixin, generic.CreateView):
    template_name = "products/admin/stock/create.html"
    page_title = "Ajouter un stock"
    success_message = "Stock « %(object)s » ajouté."
    error_message = "Le stock ne peut être créé du fait d'erreurs."

    panels = [
        FieldPanel("user"),
        FieldPanel("product"),
        FieldPanel("amount"),
        FieldPanel("part_percentage"),
    ]
    base_form_class = UserStockCreateForm

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.for_user = self.request.GET.get("user", None)

    def get_initial(self):
        initial = super().get_initial()

        if self.for_user is not None:
            initial["user"] = self.for_user

        if "product" in self.request.GET:
            initial["product"] = self.request.GET["product"]

        return initial

    def get_index_url(self):
        index_url = super().get_index_url()
        # Garde le même producteur⋅rice pour l'index des stocks
        if self.for_user is not None:
            index_url += "?user=%d" % int(self.for_user)
        return index_url

    def get_success_url(self):
        return reverse(
            "shopadmin_product:inspect", args=(quote(self.object.product_id),)
        )


class StockEditView(ShopBreadcrumbsMixin, generic.EditView):
    success_message = "Stock « %(object)s » mis à jour."
    error_message = "Le stock ne peut être enregistré du fait d'erreurs."

    panels = [
        FieldPanel("amount"),
        FieldPanel("part_percentage"),
    ]

    def get_queryset(self):
        return super().get_queryset().select_related("product", "user")

    def get_index_url(self):
        index_url = super().get_index_url()
        # Garde le même producteur⋅rice pour l'index des stocks
        index_url += "?user=%d" % int(self.object.user_id)
        return index_url

    def get_success_url(self):
        return reverse(
            "shopadmin_product:inspect", args=(quote(self.object.product_id),)
        )


class StockDeleteView(ShopBreadcrumbsMixin, generic.DeleteView):
    confirmation_message = "Êtes-vous sûr⋅e de vouloir supprimer ce stock ?"
    success_message = "Stock « %(object)s » supprimé."

    def get_queryset(self):
        return super().get_queryset().select_related("product", "user")

    def get_index_url(self):
        index_url = super().get_index_url()
        # Garde le même producteur⋅rice pour l'index des stocks
        index_url += "?user=%d" % int(self.object.user_id)
        return index_url


class StockViewSet(generic.ViewSet):
    icon = "boxes"
    model = Stock
    permission_policy = stock_permission_policy

    index_view_class = StockIndexView
    add_view_class = StockCreateView
    edit_view_class = StockEditView
    delete_view_class = StockDeleteView


# ORDERS
# ------------------------------------------------------------------------------


class OrderIndexView(
    PermissionCheckedMixin, WagtailAdminTemplateMixin, FormView
):
    template_name = "products/admin/order/index.html"
    form_class = OrdersImportForm
    page_title = "Commandes"
    any_permission_required = ["add", "change", "delete"]

    index_url_name = None

    def get_index_url(self):
        return reverse(self.index_url_name)

    def get_page_subtitle(self):
        if "rapport" in self.request.FILES:
            return self.request.FILES["rapport"].name
        return ""

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["index_url"] = self.get_index_url()
        return context

    def form_invalid(self, form):
        messages.validation_error(
            self.request,
            (
                "La répartition des commandes ne peut être générée du "
                "fait d'erreurs."
            ),
            form,
        )
        return super().form_invalid(form)

    def form_valid(self, form):
        product_orders = form.cleaned_data["product_orders"]
        order_cycles = form.cleaned_data["order_cycles"]

        errors = []

        # Répartis en fonction des stocks disponibles
        for product_order in product_orders:
            product = product_order.product

            try:
                product_order.stocks = Stock.objects.divide_between_producers(
                    product, product_order.total_amount
                )
            except ValueError as e:
                errors.append(ValidationError(e))

        if errors:
            form.add_error(None, ValidationError(errors))
            return self.form_invalid(form)

        reports = [
            (
                "Points de retrait",
                self.generate_order_cycles_reports(product_orders),
            ),
            (
                "Répartition",
                self.generate_producers_reports(product_orders, order_cycles),
            ),
        ]

        return self.render_to_response(
            self.get_context_data(form=form, reports=reports)
        )

    def generate_order_cycles_reports(self, product_orders):
        headers = ["Quantité", "Produit", "Variante"]

        order_cycles = {}

        for product_order in product_orders:
            product = product_order.product

            for order_cycle, amount in product_order.orders.items():
                order_cycles.setdefault(order_cycle, [])
                order_cycles[order_cycle].append(
                    (
                        amount,
                        product.name,
                        product.variant_display,
                    )
                )

        reports = []  # [(name, path)]

        for order_cycle, data in order_cycles.items():
            name = "{} - {}".format(order_cycle.place, order_cycle.date)
            reports.append(
                (
                    name,
                    self.generate_report(
                        "products/reports/order_cycle.html",
                        {
                            "name": name,
                            "headers": headers,
                            "data": data,
                        },
                        slugify(name),
                    ),
                )
            )

        return reports

    def generate_producers_reports(self, product_orders, order_cycles):
        headers = ["Quantité", "Produit", "Variante"] + [
            order_cycle.code for order_cycle in order_cycles
        ]

        producers = {}

        for product_order in product_orders:
            product = product_order.product
            orders = product_order.orders.copy()

            for stock in product_order.stocks:
                producers.setdefault(stock.user_id, [])

                data = [stock.supplied, product.name, product.variant_display]

                remaining = stock.supplied

                # Répartis entre les points de retrait
                for order_cycle in order_cycles:
                    order_cycle_amount = 0

                    if remaining and order_cycle in orders:
                        order_cycle_amount = (
                            remaining
                            if orders[order_cycle] > remaining
                            else orders[order_cycle]
                        )

                        remaining -= order_cycle_amount
                        orders[order_cycle] -= order_cycle_amount

                    data.append(order_cycle_amount)

                producers[stock.user_id].append(data)

        users = OrderedDict(
            (u.id, u.get_full_name())
            for u in (
                User.objects.filter(id__in=producers.keys()).order_by(
                    "last_name"
                )
            )
        )

        reports = []  # [(name, path)]

        for user_id, full_name in users.items():
            name = "Récapitulatif de {}".format(full_name)
            reports.append(
                (
                    name,
                    self.generate_report(
                        "products/reports/producer.html",
                        {
                            "name": name,
                            "headers": headers,
                            "data": producers[user_id],
                            "order_cycles": order_cycles,
                        },
                        slugify(name),
                    ),
                )
            )

        reports.append(
            (
                "Par produits (au format CSV)",
                self.generate_products_division(
                    product_orders, users, "repartition"
                ),
            )
        )

        return reports

    def generate_report(self, template_name, context, filename):
        name = "products/reports/{}/{}.pdf".format(
            datetime.date.today().isoformat(), filename
        )
        content = ContentFile(b"")

        html = get_template(template_name).render(context)
        HTML(string=html).write_pdf(content)

        name = default_storage.save(name, content)
        return default_storage.url(name)

    def generate_products_division(self, product_orders, users, filename):
        name = "products/reports/{}/{}.csv".format(
            datetime.date.today().isoformat(), filename
        )
        content = ContentFile("")

        writer = csv.writer(content)
        writer.writerow(
            ["Produit", "Variante", "Prix unitaire", "Quantité"]
            + list(users.values())
        )

        for product_order in product_orders:
            product = product_order.product
            producers_stock = {
                stock.user_id: stock.supplied for stock in product_order.stocks
            }

            row = [
                product.name,
                product.variant_display,
                product_order.unit_price,
                product_order.total_amount,
            ]

            for user_id in users.keys():
                row.append(producers_stock.get(user_id, 0))

            writer.writerow(row)

        name = default_storage.save(name, content)
        return default_storage.url(name)


class OrderViewSet(ViewSet):
    icon = "basket"
    permission_policy = stock_permission_policy

    index_view_class = OrderIndexView

    @property
    def index_view(self):
        return self.index_view_class.as_view(
            permission_policy=self.permission_policy,
            index_url_name=self.get_url_name("index"),
            header_icon=self.icon,
        )

    def get_urlpatterns(self):
        return super().get_urlpatterns() + [
            path("", self.index_view, name="index"),
        ]
