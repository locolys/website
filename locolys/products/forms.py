import csv
from decimal import Decimal

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.utils.safestring import mark_safe

from wagtail.admin.forms.models import WagtailAdminModelForm

from .models import OrderCycle, Product, ProductOrder, Stock


class UserStockCreateForm(WagtailAdminModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if "user" in self.initial:
            product = self.fields["product"]
            product.queryset = product.queryset.exclude(
                stock__user=self.initial["user"]
            )

        if "product" in self.initial:
            user = self.fields["user"]
            user.queryset = user.queryset.exclude(
                product_stock__product=self.initial["product"]
            )


class StockUpdateForm(WagtailAdminModelForm):
    class Meta:
        model = Stock
        fields = ["amount", "part_percentage"]


class OrdersImportForm(forms.Form):
    rapport = forms.FileField(
        label="Rapport (au format CSV)",
        help_text=mark_safe(
            "Ce fichier est à générer depuis l'administration de "
            "CoopCircuits : dans l'onglet <i>Rapports</i>, sélectionnez <i>"
            "Rapports des commandes / Totaux Cycle de Vente par Acheteur</i> "
            "et renseignez la période et le(s) cycle(s) de vente désiré(s)."
        ),
        validators=[
            FileExtensionValidator(
                allowed_extensions=["csv"],
                message="Le fichier doit être au format CSV.",
            )
        ],
        widget=forms.FileInput,
    )

    def clean_rapport(self):
        file = self.cleaned_data["rapport"]

        if file.multiple_chunks():
            raise ValidationError("Le fichier semble contenir trop de lignes.")

        lines = file.read().decode().strip().split("\n")
        return csv.reader(lines[1:])

    def clean(self):
        cleaned_data = super().clean()

        if "rapport" not in cleaned_data:
            return cleaned_data

        rows = list(cleaned_data["rapport"])

        try:
            order_cycles = {
                order_cycle: OrderCycle.from_string(order_cycle)
                for order_cycle in {row[24] for row in rows}
            }
        except ValueError as e:
            raise ValidationError([e])

        errors = []

        products = {}  # (name, variant_display): ProductOrder

        for row in rows:
            amount = int(row[7])
            order_cycle = order_cycles[row[24]]

            key = (row[5], row[6])

            try:
                product = products[key]
            except KeyError:
                product = ProductOrder(unit_price=Decimal(row[8]) / amount)
                products[key] = product

            product.add_order(order_cycle, amount)

        product_orders = []

        # Récupère les objets Product correspondants
        for key, product_order in products.items():
            (name, variant_display) = key

            try:
                product_order.product = Product.objects.get(
                    name=name, variant_display=variant_display
                )
            except Product.DoesNotExist:
                errors.append(
                    f"Le produit « {name} / {variant_display} » n'existe pas."
                )
            else:
                product_orders.append(product_order)

        if errors:
            raise ValidationError(errors)

        cleaned_data["product_orders"] = sorted(
            product_orders, key=lambda o: str(o.product)
        )
        cleaned_data["order_cycles"] = sorted(
            order_cycles.values(), key=lambda o: o.code
        )

        return cleaned_data
