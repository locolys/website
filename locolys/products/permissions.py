from wagtail.permissions import ModelPermissionPolicy

from locolys.admin.permissions import OwnerModelPermissionPolicy

from .models import Product, Stock

product_permission_policy = ModelPermissionPolicy(Product)
stock_permission_policy = OwnerModelPermissionPolicy(Stock)
