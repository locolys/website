from django.contrib.auth.models import Permission
from django.urls import reverse

from wagtail import hooks
from wagtail.admin.menu import Menu, SubmenuMenuItem

from locolys.admin.menu import PermissionMenuItem

from .views.admin import OrderViewSet, ProductViewSet, StockViewSet

shop_menu = Menu(register_hook_name="register_shop_menu_item")


@hooks.register("register_admin_menu_item")
def register_shop_menu():
    return SubmenuMenuItem(
        "Boutique",
        shop_menu,
        icon_name="shop",
        order=410,
    )


@hooks.register("register_shop_menu_item")
def register_product_menu_item():
    return PermissionMenuItem(
        "Produits",
        reverse("shopadmin_product:index"),
        ProductViewSet.permission_policy,
        permissions=["view", "add", "change", "delete"],
        icon_name=ProductViewSet.icon,
        order=10,
    )


@hooks.register("register_shop_menu_item")
def register_stock_menu_item():
    return PermissionMenuItem(
        "Stocks",
        reverse("shopadmin_stock:index"),
        StockViewSet.permission_policy,
        icon_name=StockViewSet.icon,
        order=20,
    )


@hooks.register("register_shop_menu_item")
def register_order_menu_item():
    return PermissionMenuItem(
        "Commandes",
        reverse("shopadmin_order:index"),
        OrderViewSet.permission_policy,
        icon_name=OrderViewSet.icon,
        order=30,
    )


@hooks.register("register_admin_viewset")
def register_order_viewset():
    return OrderViewSet("shopadmin_order", url_prefix="shop/order")


@hooks.register("register_admin_viewset")
def register_product_viewset():
    return ProductViewSet("shopadmin_product", url_prefix="shop/product")


@hooks.register("register_admin_viewset")
def register_stock_viewset():
    return StockViewSet("shopadmin_stock", url_prefix="shop/stock")


@hooks.register("register_permissions")
def register_permissions():
    return Permission.objects.filter(
        codename__in=[
            "view_product",
            "add_product",
            "change_product",
            "delete_product",
            "add_stock",
            "change_stock",
            "delete_stock",
        ],
    )


@hooks.register("register_icons")
def register_icons(icons):
    for icon in ["carrot", "basket", "boxes", "shop"]:
        icons.append("products/icons/{}.svg".format(icon))
    return icons
