MASS_UNITS = ["g", "kg", "T"]
VOLUME_UNITS = ["mL", "L", "kL"]


def pluralize(word, number, suffix="s"):
    if float(number) > 1 and word[-1] != suffix:
        return f"{word}{suffix}"
    return word


def convert_to_lower_unit(amount, unit):
    try:
        index = MASS_UNITS.index(unit)
        units = MASS_UNITS
    except ValueError:
        try:
            index = VOLUME_UNITS.index(unit)
            units = VOLUME_UNITS
        except ValueError:
            raise ValueError("Unknown unit {}".format(unit))

    prev_index = index - 1

    if prev_index >= 0:
        amount = amount * 1000
        index = prev_index

    return (amount, units[index])


def format_amount(amount, unit):
    if amount < 1:
        amount, unit = convert_to_lower_unit(amount, unit)
    return "{0:.3g}{1}".format(amount, unit)
