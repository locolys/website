from decimal import Decimal

import factory
from factory.django import DjangoModelFactory

from .. import models


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = models.Product

    name = factory.Sequence(lambda x: "Produit #{0}".format(x))
    category = "LÉGUMES"
    units = Decimal("1")
    variant_unit_name = "pièce"
    price = Decimal("2.5")
    on_demand = False


class StockFactory(DjangoModelFactory):
    class Meta:
        model = models.Stock

    product = factory.SubFactory(ProductFactory)
