from decimal import Decimal

import pytest

from locolys.tests.factories import UserFactory

from ..models import Stock
from .factories import ProductFactory, StockFactory


@pytest.fixture
def producer1():
    return UserFactory(
        email="producer1@example.org",
        first_name="Producer 1",
    )


@pytest.fixture
def producer2():
    return UserFactory(
        email="producer2@example.org",
        first_name="Producer 2",
    )


class TestProduct:
    @pytest.mark.parametrize(
        "props,result",
        [
            (
                {},
                "Produit / 1 pièce",
            ),
            (
                {"units": Decimal("10.000")},
                "Produit / 10 pièces",
            ),
            (
                {"variant_unit_name": "pièces", "units": Decimal("10")},
                "Produit / 10 pièces",
            ),
            (
                {"display_name": "Variante", "units": Decimal("2")},
                "Produit / Variante (2 pièces)",
            ),
            (
                {"display_as": "500g"},
                "Produit / 500g",
            ),
            (
                {"display_name": "Variante", "display_as": "500g"},
                "Produit / Variante (500g)",
            ),
        ],
    )
    def test_str__piece(self, props, result):
        assert str(ProductFactory.build(name="Produit", **props)) == result

    @pytest.mark.parametrize(
        "props,result",
        [
            (
                {},
                "Produit / 1kg",
            ),
            (
                {"units": Decimal("0.5")},
                "Produit / 500g",
            ),
            (
                {"units": Decimal("0.5"), "unit_type": "g"},
                "Produit / 0.5g",
            ),
            (
                {"display_name": "Variante", "units": Decimal("2")},
                "Produit / Variante (2kg)",
            ),
            (
                {"display_as": "500g"},
                "Produit / 500g",
            ),
            (
                {"display_name": "Variante", "display_as": "500g"},
                "Produit / Variante (500g)",
            ),
        ],
    )
    def test_str__mass(self, props, result):
        props.setdefault("unit_type", "kg")
        assert str(ProductFactory.build(name="Produit", **props)) == result

    @pytest.mark.parametrize(
        "props,result",
        [
            (
                {},
                "Produit / 1L",
            ),
            (
                {"units": Decimal("0.5")},
                "Produit / 500mL",
            ),
            (
                {"units": Decimal("0.5"), "unit_type": "mL"},
                "Produit / 0.5mL",
            ),
            (
                {"display_name": "Variante", "units": Decimal("2")},
                "Produit / Variante (2L)",
            ),
            (
                {"display_as": "500mL"},
                "Produit / 500mL",
            ),
            (
                {"display_name": "Variante", "display_as": "500mL"},
                "Produit / Variante (500mL)",
            ),
        ],
    )
    def test_str__volume(self, props, result):
        props.setdefault("unit_type", "L")
        assert str(ProductFactory.build(name="Produit", **props)) == result

    def test_unit_display_unknown(self):
        with pytest.raises(ValueError):
            product = ProductFactory.build(
                unit_type="pouet", units=Decimal("0.5")
            )
            product.get_unit_display()


@pytest.mark.django_db
class TestStockManager:
    def test_divide_no_stock(self):
        product = ProductFactory()

        with pytest.raises(ValueError, match="Aucun stock"):
            Stock.objects.divide_between_producers(product, 10)

    def test_divide_not_enough_stock(self, producer1, producer2):
        product = ProductFactory()
        StockFactory(
            product=product, user=producer1, amount=2, part_percentage=50
        )
        StockFactory(
            product=product, user=producer2, amount=2, part_percentage=50
        )

        with pytest.raises(ValueError, match="6 manquant"):
            Stock.objects.divide_between_producers(product, 10)

    def test_divide_unique_producer(self, producer1):
        stock = StockFactory(user=producer1, amount=20)

        results = Stock.objects.divide_between_producers(stock.product, 9)
        assert len(results) == 1
        new_stock = results[0]
        assert new_stock.id == stock.id
        assert new_stock.amount == 11
        assert new_stock.supplied == 9

    def test_divide_multiple_producer(self, producer1, producer2):
        product = ProductFactory()
        stock1 = StockFactory(
            product=product, user=producer1, amount=20, part_percentage=50
        )
        stock2 = StockFactory(
            product=product, user=producer2, amount=20, part_percentage=50
        )

        results = Stock.objects.divide_between_producers(product, 10)
        assert len(results) == 2
        new_stock1 = results[0]
        assert new_stock1.id == stock1.id
        assert new_stock1.amount == 15
        assert new_stock1.supplied == 5
        new_stock2 = results[1]
        assert new_stock2.id == stock2.id
        assert new_stock2.amount == 15
        assert new_stock2.supplied == 5

        stock1.part_percentage = 33
        stock1.save()
        stock2.part_percentage = 66
        stock2.save()

        results = Stock.objects.divide_between_producers(product, 10)
        assert len(results) == 2
        new_stock2 = results[0]
        assert new_stock2.id == stock2.id
        assert new_stock2.amount == 13
        assert new_stock2.supplied == 7
        new_stock1 = results[1]
        assert new_stock1.id == stock1.id
        assert new_stock1.amount == 17
        assert new_stock1.supplied == 3

    def test_divide_multiple_producer_completed(self, producer1, producer2):
        product = ProductFactory()
        stock1 = StockFactory(
            product=product, user=producer1, amount=10, part_percentage=50
        )
        stock2 = StockFactory(
            product=product, user=producer2, amount=2, part_percentage=50
        )

        results = Stock.objects.divide_between_producers(product, 10)
        assert len(results) == 2
        new_stock1 = results[0]
        assert new_stock1.id == stock1.id
        assert new_stock1.amount == 2
        assert new_stock1.supplied == 8
        new_stock2 = results[1]
        assert new_stock2.id == stock2.id
        assert new_stock2.amount == 0
        assert new_stock2.supplied == 2
