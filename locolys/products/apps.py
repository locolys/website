from django.apps import AppConfig


class ProductsConfig(AppConfig):
    name = "locolys.products"
    verbose_name = "Produits"

    def ready(self):
        from .panels import set_models_panels

        set_models_panels()
