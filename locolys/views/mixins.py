class PageTemplateMixin:
    """
    Définis les propriétés nécessaires pour le rendu du template d'une page.
    """

    #: Le titre de la page affiché dans l'en-tête.
    title = ""

    #: L'introduction de la page affichée dans l'en-tête (optionnel).
    introduction = ""

    #: Le titre de la page pour les méta données (optionnel).
    seo_title = ""

    def get_title(self):
        return self.title

    def get_introduction(self):
        return self.introduction

    def get_seo_title(self):
        return self.seo_title or self.get_title()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = self.get_title()
        context["introduction"] = self.get_introduction()
        context["seo_title"] = self.get_seo_title()
        return context
