from django.conf import settings
from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.views.generic import UpdateView

from ..forms.account import (
    AuthenticationForm,
    EmailChangeForm,
    PasswordChangeForm,
    PasswordResetForm,
    ProfileUpdateForm,
    SetPasswordForm,
)
from ..forms.newsletter import UserNewsletterSubscriptionForm
from ..models import NewsletterSubscription
from ..ui import LinkMenuItem
from .mixins import PageTemplateMixin


class LoginView(PageTemplateMixin, auth_views.LoginView):
    authentication_form = AuthenticationForm
    template_name = "account/login.html"

    title = "Connexion"


class LogoutView(auth_views.LogoutView):
    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        messages.success(request, "Vous avez été déconnecté du site.")
        return response


class PasswordResetView(PageTemplateMixin, auth_views.PasswordResetView):
    form_class = PasswordResetForm
    success_url = settings.LOGIN_URL
    template_name = "account/password_reset.html"
    email_template_name = "account/password_reset_email_body.txt"
    subject_template_name = "account/password_reset_email_subject.txt"

    title = "Mot de passe oublié"
    introduction = (
        "Un courriel vous sera envoyé avec les instructions à suivre pour "
        "réinitialiser votre mot de passe."
    )

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.info(
            self.request,
            format_html(
                (
                    "Un courriel a été envoyé à <i>{}</i>, si cette adresse "
                    "mail est valide. Il contient les instructions à suivre "
                    "pour réinitialiser votre mot de passe."
                ),
                form.cleaned_data["email"],
            ),
        )
        return response


class PasswordResetConfirmView(
    PageTemplateMixin, auth_views.PasswordResetConfirmView
):
    form_class = SetPasswordForm
    success_url = settings.LOGIN_URL
    template_name = "account/password_reset_confirm.html"

    title = "Définir mon mot de passe"

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(
            self.request,
            (
                "Votre mot de passe a été réinitialisé, vous pouvez "
                "désormais vous connecter."
            ),
        )
        return response


# MON COMPTE
# ------------------------------------------------------------------------------

profile_menu_item = LinkMenuItem(
    "profile",
    "Mon compte",
    reverse_lazy("account:profile_update"),
    icon_name="person-square",
)
subscriptions_menu_item = LinkMenuItem(
    "subscriptions",
    "Mes abonnements",
    reverse_lazy("account:subscriptions_update"),
    icon_name="bell",
)
# orders_menu_item = LinkMenuItem(
#     'orders',
#     "Mes commandes",
#     reverse_lazy('account:orders'),
#     icon_name='basket',
# )


class AccountPageMixin(PageTemplateMixin):
    account_menu_items = [profile_menu_item, subscriptions_menu_item]

    #: L'object `LinkMenuItem` de l'élément de menu du compte actuel
    current_account_item = None

    def get_title(self):
        return self.title or self.current_account_item.label

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["account_menu_items"] = self.account_menu_items
        context["current_account_item"] = self.current_account_item
        return context


class ProfilePageMixin(AccountPageMixin):
    success_url = profile_menu_item.url
    current_account_item = profile_menu_item


class ProfileUpdateView(ProfilePageMixin, LoginRequiredMixin, UpdateView):
    form_class = ProfileUpdateForm
    template_name = "account/profile_update.html"

    def get_object(self):
        return self.request.user


class PasswordChangeView(ProfilePageMixin, auth_views.PasswordChangeView):
    form_class = PasswordChangeForm
    template_name = "account/password_change.html"

    seo_title = "Changer mon mot de passe"

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(
            self.request,
            "Votre mot de passe a été changé.",
        )
        return response


class EmailChangeView(ProfilePageMixin, LoginRequiredMixin, UpdateView):
    form_class = EmailChangeForm
    template_name = "account/email_change.html"

    seo_title = "Changer mon adresse mail"

    def get_object(self):
        return self.request.user

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(
            self.request,
            "Votre adresse mail a été changée.",
        )
        return response


class SubscriptionsUpdateView(AccountPageMixin, LoginRequiredMixin, UpdateView):
    form_class = UserNewsletterSubscriptionForm
    success_url = subscriptions_menu_item.url
    template_name = "account/subscriptions_update.html"

    current_account_item = subscriptions_menu_item

    def get_object(self):
        if hasattr(self.request.user, "newsletter_subscription"):
            return self.request.user.newsletter_subscription

        try:
            obj = NewsletterSubscription.objects.get(
                email=self.request.user.email
            )
        except NewsletterSubscription.DoesNotExist:
            return NewsletterSubscription(
                is_active=True,
                user=self.request.user,
                email=self.request.user.email,
            )

        obj.is_active = True
        obj.user = self.request.user
        return obj
