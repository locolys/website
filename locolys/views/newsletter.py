from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.utils.http import url_has_allowed_host_and_scheme
from django.views.generic import CreateView, FormView

from ..forms.newsletter import (
    NewsletterSubscribeForm,
    NewsletterUnsubscribeForm,
)
from .account import subscriptions_menu_item
from .mixins import PageTemplateMixin


class SubscribeView(PageTemplateMixin, CreateView):
    form_class = NewsletterSubscribeForm
    success_url = reverse_lazy("newsletter:subscribe")
    template_name = "newsletter/subscribe.html"

    title = "Abonnement aux infolettres"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(subscriptions_menu_item.url)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        (self.object, created) = form.save_or_delete()
        if self.object.pk is None:
            message = "Vous avez été désabonné⋅e des infolettres."
        elif not created:
            message = "Votre abonnement aux infolettres a été mis à jour."
        else:
            message = format_html(
                (
                    "Vous êtes désormais abonné⋅e aux infolettres avec "
                    "l’adresse mail <i>{}</i>."
                ),
                self.object.email,
            )
        messages.success(self.request, message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        referer = self.request.headers.get("Referer")
        # Tente de rediriger vers la page référente
        if url_has_allowed_host_and_scheme(
            referer,
            self.request.get_host(),
            require_https=self.request.is_secure(),
        ):
            return referer
        return self.success_url


class UnsubscribeView(PageTemplateMixin, FormView):
    form_class = NewsletterUnsubscribeForm
    success_url = reverse_lazy("newsletter:subscribe")
    template_name = "newsletter/unsubscribe.html"

    title = "Désabonnement aux infolettres"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(subscriptions_menu_item.url)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.delete()
        messages.success(
            self.request,
            "Vous avez été désabonné⋅e des infolettres.",
        )
        return super().form_valid(form)
