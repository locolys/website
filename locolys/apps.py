from django.apps import AppConfig


class LocolysConfig(AppConfig):
    name = "locolys"
    verbose_name = "LocoLys"
