from django import forms
from django.contrib.auth import forms as auth_forms

from ..models import User
from .mixins import TailwindTapeformMixin

PROFILE_REQUIRED_FIELDS = [
    "first_name",
    "last_name",
    "address",
    "city",
]

EMAIL_HELP_TEXT = (
    "Cette adresse mail vous sera demandée pour vous connecter au site et "
    "sera utilisée pour les notifications. Assurez-vous qu'elle est bien "
    "valide."
)
PHONE_HELP_TEXT = (
    "Votre numéro de téléphone, portable de préférence, sera uniquement "
    "utilisé pour vous contacter par rapport à votre commande (changement "
    "exceptionnel, oubli…)."
)


class AuthenticationForm(TailwindTapeformMixin, auth_forms.AuthenticationForm):
    """
    Formulaire d'authentification.
    """

    error_messages = {
        "invalid_login": (
            "L’adresse mail et le mot de passe ne correspondent pas, "
            "veuillez réessayer."
        ),
    }


class PasswordResetForm(TailwindTapeformMixin, auth_forms.PasswordResetForm):
    """
    Formulaire de demande de réinitialisation du mot de passe.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["email"].label = "Adresse mail associée à votre compte"


class SetPasswordForm(TailwindTapeformMixin, auth_forms.SetPasswordForm):
    """
    Formulaire de réinitialisation du mot de passe, sans l'ancien.
    """


# MON COMPTE
# ------------------------------------------------------------------------------


class ProfileUpdateForm(TailwindTapeformMixin, forms.ModelForm):
    """
    Formulaire de modification des champs de profil d'un compte.
    """

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "address",
            "city",
            "phone",
        ]
        help_texts = {"phone": PHONE_HELP_TEXT}

    # Configuration

    layout_template = "forms/layouts/profile_update.html"

    # Méthodes

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in PROFILE_REQUIRED_FIELDS:
            self.fields[field_name].required = True


class PasswordChangeForm(TailwindTapeformMixin, auth_forms.PasswordChangeForm):
    """
    Formulaire de changement de mot de passe, avec saisie de l'ancien.
    """


class EmailChangeForm(TailwindTapeformMixin, forms.ModelForm):
    """
    Formulaire de changement de l'adresse mail d'un compte.
    """

    class Meta:
        model = User
        fields = ["email"]
        help_texts = {"email": EMAIL_HELP_TEXT}
        widgets = {
            "email": forms.EmailInput(
                attrs={"autocomplete": "email", "autofocus": True}
            )
        }

    # Configuration

    field_label_css_class = "sr-only"
