from django import forms
from django.core.exceptions import ValidationError
from django.utils.html import format_html

from ..models import Newsletter, NewsletterSubscription
from ..utils import normalize_email
from .mixins import TailwindTapeformMixin


class NewslettersChoiceField(forms.ModelMultipleChoiceField):
    widget = forms.CheckboxSelectMultiple

    default_label_format = (
        '<b class="block">{name}</b><p class="text-sm">{description}</p>'
    )

    def __init__(self, label_format=None, label="", required=False, **kwargs):
        super().__init__(
            Newsletter.objects.all(), label=label, required=required, **kwargs
        )

        self.label_format = label_format or self.default_label_format

    def label_from_instance(self, obj):
        return format_html(
            self.label_format,
            name=obj.name,
            description=obj.description,
        )


class NewsletterSubscribeForm(TailwindTapeformMixin, forms.ModelForm):
    """
    Formulaire d'abonnement à une ou plusieurs infolettres.
    """

    class Meta:
        model = NewsletterSubscription
        fields = ["email", "newsletters"]

    email = forms.EmailField(
        label="Adresse mail",
        error_messages={"invalid": "Veuillez saisir une adresse mail valide."},
    )
    newsletters = NewslettersChoiceField()

    # Méthodes

    def save_or_delete(self):
        """
        Enregistre ou supprime l'abonnement en fonction de l'instance.
        """
        assert not self.errors

        created = self.instance.pk is None

        if not created and not self.cleaned_data["newsletters"]:
            self.instance.delete()
        else:
            self.save()

        return (self.instance, created)

    def _post_clean(self):
        if self.errors:
            return

        try:
            self.instance = NewsletterSubscription.objects.get(
                email=normalize_email(self.cleaned_data["email"])
            )
        except NewsletterSubscription.DoesNotExist:
            if not self.cleaned_data["newsletters"]:
                self.add_error(
                    "newsletters",
                    ValidationError(
                        "Veuillez choisir au moins une infolettre.",
                        code="required",
                    ),
                )

        super()._post_clean()


class NewsletterSubscribeBlockForm(NewsletterSubscribeForm):
    """
    Formulaire d'abonnement à une ou plusieurs infolettres utilisé par le
    bloc `NewsletterSubscribeBlock`.
    """

    newsletters = NewslettersChoiceField(
        label_format=(
            '<b class="underline underline-offset-2 decoration-dotted"'
            ' title="{description}">{name}</b>'
        ),
        widget=NewslettersChoiceField.widget(
            attrs={
                "data-checkbox-enabler-target": "enabler",
                "data-action": "change->checkbox-enabler#update",
            },
        ),
    )

    # Configuration

    def get_field_container_css_class(self, bound_field):
        if bound_field.name == "newsletters":
            return "mt-2 text-sm leading-normal"
        return super().get_field_container_css_class(bound_field)

    def get_widget_css_class(self, field_name, field):
        if field_name == "newsletters":
            return "flex flex-wrap justify-center items-center gap-4 2xl:gap-8"
        return super().get_widget_css_class(field_name, field)


class UserNewsletterSubscriptionForm(TailwindTapeformMixin, forms.ModelForm):
    """
    Formulaire de modification des abonnements aux infolettres d'un compte.
    """

    class Meta:
        model = NewsletterSubscription
        fields = ["newsletters", "email"]

    email = forms.EmailField(
        label="Adresse mail de réception des infolettres",
        error_messages={"invalid": "Veuillez saisir une adresse mail valide."},
    )
    newsletters = NewslettersChoiceField()


class NewsletterUnsubscribeForm(TailwindTapeformMixin, forms.Form):
    """
    Formulaire de désabonnement d'une adresse mail aux infolettres.
    """

    email = forms.EmailField(
        label="Adresse mail",
        error_messages={
            "required": "Veuillez saisir l’adresse mail à désabonner.",
            "invalid": "Veuillez saisir une adresse mail valide.",
        },
    )

    # Méthodes

    def delete(self):
        assert not self.errors

        return NewsletterSubscription.objects.filter(
            email__iexact=self.cleaned_data["email"].strip()
        ).delete()
