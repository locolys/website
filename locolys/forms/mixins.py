from django import forms

from tapeforms.mixins import TapeformMixin


class TailwindTapeformMixin(TapeformMixin):
    layout_template = "forms/layouts/default.html"

    field_template = "forms/fields/default.html"
    field_container_css_class = "mb-4"
    field_label_css_class = "inline-block text-gray-900"
    widget_css_class = "block w-full mt-2"
    widget_invalid_css_class = "border-error focus:shadow-error/50"

    widget_template_overrides = {
        forms.CheckboxSelectMultiple: "forms/widgets/checkbox_select.html",
    }

    def get_widget_css_class(self, field_name, field):
        if field.widget.__class__ == forms.CheckboxInput:
            return ""

        if field.widget.__class__ in [
            forms.RadioSelect,
            forms.CheckboxSelectMultiple,
        ]:
            return "space-y-4"

        return self.widget_css_class
