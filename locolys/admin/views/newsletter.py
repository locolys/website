from wagtail.admin.ui.tables import Column, DateColumn, TitleColumn
from wagtail.admin.views.reports import ReportView

from locolys.models import Newsletter, NewsletterSubscription

from ..filters import NewsletterSubscriptionsFilterSet
from ..permissions import (
    newsletter_permission_policy,
    newsletter_subscription_permission_policy,
)
from . import generic


class NewsletterIndexView(generic.IndexView):
    page_title = "Infolettres"
    add_item_label = "Ajouter une infolettre"
    default_ordering = "name"

    columns = [
        TitleColumn(
            "name",
            label="Nom",
            sort_key="name",
            url_name="admin_newsletter:edit",
        ),
        Column(
            "subscriptions",
            label="Abonnements",
            accessor=lambda o: o.subscriptions.count(),
        ),
    ]


class NewsletterCreateView(generic.CreateView):
    page_title = "Ajouter une infolettre"
    success_message = "Infolettre « %(object)s » ajoutée."
    error_message = "L’infolettre ne peut être créée du fait d'erreurs."


class NewsletterEditView(generic.EditView):
    success_message = "Infolettre « %(object)s » mise à jour."
    error_message = "L’infolettre ne peut être enregistrée du fait d'erreurs."


class NewsletterDeleteView(generic.DeleteView):
    confirmation_message = (
        "Êtes-vous sûr⋅e de vouloir supprimer cette infolettre ?"
    )
    success_message = "Infolettre « %(object)s » supprimée."


class NewsletterViewSet(generic.ViewSet):
    icon = "newspaper"
    model = Newsletter
    permission_policy = newsletter_permission_policy

    index_view_class = NewsletterIndexView
    add_view_class = NewsletterCreateView
    edit_view_class = NewsletterEditView
    delete_view_class = NewsletterDeleteView


# REPORTS
# ------------------------------------------------------------------------------


class NewsletterSubscriptionsReportView(ReportView):
    permission_policy = newsletter_subscription_permission_policy
    permission_required = "view"

    columns = [
        Column("email", label="Adresse mail"),
        DateColumn("date_subscribed", label="Date d’abonnement"),
    ]
    filterset_class = NewsletterSubscriptionsFilterSet
    header_icon = "newspaper"
    title = "Abonnements aux infolettres"
    template_name = "wagtailadmin/reports/newsletter_subscriptions.html"

    export_headings = {"email": "Adresse mail"}
    list_export = ["email"]

    def get_queryset(self):
        return (
            NewsletterSubscription.objects.all()
            .filter(is_active=True)
            .order_by("-date_subscribed")
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["subtitle"] = "(%s)" % context["paginator"].count
        return context
