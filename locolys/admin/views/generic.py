from django.contrib.admin.utils import display_for_field, label_for_field, quote
from django.core.exceptions import PermissionDenied
from django.template.defaultfilters import capfirst
from django.urls import path, reverse
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectMixin

from wagtail.admin.panels import ObjectList, get_edit_handler
from wagtail.admin.utils import get_latest_str
from wagtail.admin.views import generic
from wagtail.admin.views.generic.base import WagtailAdminTemplateMixin
from wagtail.admin.views.generic.permissions import PermissionCheckedMixin
from wagtail.admin.viewsets.model import ModelViewSet

# == Mixins ==


class PanelMixin:
    base_form_class = None
    panels = []

    def get_panel(self):
        if self.panel:
            return self.panel
        if self.panels:
            return ObjectList(
                self.panels,
                base_form_class=self.base_form_class,
            ).bind_to_model(self.model)
        # Retrieve the panel from the model
        return get_edit_handler(self.model)


class BreadcrumbsMixin:
    breadcrumbs_items = []
    breadcrumbs_is_expanded = False

    def get_breadcrumbs_items(self):
        return self.breadcrumbs_items

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs_items"] = self.get_breadcrumbs_items()
        context["breadcrumbs_is_expanded"] = self.breadcrumbs_is_expanded
        return context


class InstancePermissionCheckedMixin(PermissionCheckedMixin):
    #: An action name the user must have on an instance.
    permission_required_for_instance = None

    #: A list of action names the user must have on an instance.
    any_permission_required_for_instance = None

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)

        if getattr(self, "permission_policy", None) is not None:
            if self.permission_required_for_instance is not None:
                if not self.user_has_permission_for_instance(
                    self.permission_required_for_instance, obj
                ):
                    raise PermissionDenied

            if self.any_permission_required_for_instance is not None:
                if not self.user_has_any_permission_for_instance(
                    self.any_permission_required_for_instance, obj
                ):
                    raise PermissionDenied

        return obj

    def user_has_permission_for_instance(self, permission, obj):
        return self.permission_policy.user_has_permission_for_instance(
            self.request.user, permission, obj
        )

    def user_has_any_permission_for_instance(self, permissions, obj):
        return self.permission_policy.user_has_any_permission_for_instance(
            self.request.user, permissions, obj
        )


# == Views ==


class IndexView(BreadcrumbsMixin, generic.IndexView):
    template_name = "admin/generic/index.html"
    results_template_name = "admin/generic/index_results.html"

    def get_breadcrumbs_items(self):
        return super().get_breadcrumbs_items() + [
            {"label": capfirst(self.model._meta.verbose_name_plural)},
        ]


class CreateView(BreadcrumbsMixin, PanelMixin, generic.CreateView):
    template_name = "admin/generic/create.html"

    def get_index_url(self):
        return reverse(self.index_url_name)

    def get_breadcrumbs_items(self):
        return super().get_breadcrumbs_items() + [
            {
                "label": capfirst(self.model._meta.verbose_name_plural),
                "url": self.get_index_url(),
            },
            {"label": "Ajouter"},
        ]


class EditView(
    InstancePermissionCheckedMixin,
    BreadcrumbsMixin,
    PanelMixin,
    generic.EditView,
):
    permission_required = None
    permission_required_for_instance = "change"
    template_name = "admin/generic/edit.html"

    inspect_url_name = None

    def get_index_url(self):
        return reverse(self.index_url_name)

    def get_inspect_url(self):
        if self.inspect_url_name:
            return reverse(self.inspect_url_name, args=(quote(self.object.pk),))

    def get_breadcrumbs_items(self):
        return super().get_breadcrumbs_items() + [
            {
                "label": capfirst(self.model._meta.verbose_name_plural),
                "url": self.get_index_url(),
            },
            {
                "label": get_latest_str(self.object),
                "url": self.get_inspect_url(),
            },
            {"label": "Modifier"},
        ]


class DeleteView(
    InstancePermissionCheckedMixin, BreadcrumbsMixin, generic.DeleteView
):
    permission_required = None
    permission_required_for_instance = "delete"
    template_name = "admin/generic/confirm_delete.html"

    inspect_url_name = None

    def get_index_url(self):
        return reverse(self.index_url_name)

    def get_inspect_url(self):
        if self.inspect_url_name:
            return reverse(self.inspect_url_name, args=(quote(self.object.pk),))

    def get_breadcrumbs_items(self):
        return super().get_breadcrumbs_items() + [
            {
                "label": capfirst(self.model._meta.verbose_name_plural),
                "url": self.get_index_url(),
            },
            {
                "label": get_latest_str(self.object),
                "url": self.get_inspect_url(),
            },
            {"label": "Supprimer"},
        ]


class InspectView(
    InstancePermissionCheckedMixin,
    BreadcrumbsMixin,
    WagtailAdminTemplateMixin,
    SingleObjectMixin,
    TemplateView,
):
    permission_required_for_instance = "view"
    template_name = "admin/generic/inspect.html"
    index_url_name = None
    edit_url_name = None
    edit_item_label = "Modifier"
    delete_url_name = None
    delete_item_label = "Supprimer"

    #: The name of fields to display. To display multiple fields on the
    #: same line, wrap those fields in their own tuple.
    fields = []

    #: The default display value for record's fields that are empty.
    empty_value_display = "-"

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.object = self.get_object()
        self.fields = self.get_fields()

    def get_page_title(self):
        if not self.page_title:
            return capfirst(self.model._meta.verbose_name)

    def get_page_subtitle(self):
        return str(self.object)

    def get_index_url(self):
        if self.index_url_name:
            return reverse(self.index_url_name)

    def get_edit_url(self):
        if self.edit_url_name:
            return reverse(self.edit_url_name, args=(quote(self.object.pk),))

    def get_delete_url(self):
        if self.delete_url_name:
            return reverse(self.delete_url_name, args=(quote(self.object.pk),))

    def get_field_label(self, field_name, field):
        """Return a label to display for a field."""
        return label_for_field(field_name, model=self.model)

    def get_field_display_value(self, field_name, field):
        """Return a display value for a field/attribute."""
        value = getattr(self.object, field_name)
        display = display_for_field(value, field, self.empty_value_display)

        if display == "":
            return self.empty_value_display
        return display

    def get_context_for_field(self, field_name):
        field = self.model._meta.get_field(field_name)
        return {
            "name": field_name,
            "label": self.get_field_label(field_name, field),
            "value": self.get_field_display_value(field_name, field),
        }

    def get_fields(self):
        return self.fields

    def get_fields_context(self):
        rows = []

        for field_name in self.fields:
            if not hasattr(field_name, "__iter__") or isinstance(
                field_name, str
            ):
                rows.append([self.get_context_for_field(field_name)])
            else:
                rows.append([self.get_context_for_field(f) for f in field_name])

        return rows

    def get_breadcrumbs_items(self):
        return super().get_breadcrumbs_items() + [
            {
                "label": capfirst(self.model._meta.verbose_name_plural),
                "url": self.get_index_url(),
            },
            {"label": get_latest_str(self.object)},
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["fields"] = self.get_fields_context()
        context["can_edit"] = (
            self.permission_policy is None
            or self.permission_policy.user_has_permission(
                self.request.user, "change"
            )
        )
        if context["can_edit"]:
            context["edit_url"] = self.get_edit_url()
            context["edit_item_label"] = self.edit_item_label
        context["can_delete"] = (
            self.permission_policy is None
            or self.permission_policy.user_has_permission(
                self.request.user, "delete"
            )
        )
        if context["can_delete"]:
            context["delete_url"] = self.get_delete_url()
            context["delete_item_label"] = self.delete_item_label
        return context


# == ViewSet ==


class ViewSet(ModelViewSet):
    index_view_class = IndexView
    add_view_class = CreateView
    edit_view_class = EditView
    delete_view_class = DeleteView

    inspect_view_class = InspectView
    inspect_view_enabled = False

    @property
    def add_view(self):
        return self.add_view_class.as_view(
            model=self.model,
            permission_policy=self.permission_policy,
            index_url_name=self.get_url_name("index"),
            add_url_name=self.get_url_name("add"),
            edit_url_name=self.get_url_name("edit"),
            header_icon=self.icon,
        )

    @property
    def edit_view(self):
        return self.edit_view_class.as_view(
            model=self.model,
            permission_policy=self.permission_policy,
            index_url_name=self.get_url_name("index"),
            edit_url_name=self.get_url_name("edit"),
            delete_url_name=self.get_url_name("delete"),
            inspect_url_name=(
                self.get_url_name("inspect")
                if self.inspect_view_enabled
                else None
            ),
            header_icon=self.icon,
        )

    @property
    def delete_view(self):
        return self.delete_view_class.as_view(
            model=self.model,
            permission_policy=self.permission_policy,
            index_url_name=self.get_url_name("index"),
            delete_url_name=self.get_url_name("delete"),
            inspect_url_name=(
                self.get_url_name("inspect")
                if self.inspect_view_enabled
                else None
            ),
            header_icon=self.icon,
        )

    @property
    def inspect_view(self):
        return self.inspect_view_class.as_view(
            model=self.model,
            permission_policy=self.permission_policy,
            index_url_name=self.get_url_name("index"),
            edit_url_name=self.get_url_name("edit"),
            delete_url_name=self.get_url_name("delete"),
            header_icon=self.icon,
        )

    def get_urlpatterns(self):
        urlpatterns = [
            path("", self.index_view, name="index"),
            path("results/", self.index_results_view, name="index_results"),
            path("add/", self.add_view, name="add"),
            path("<int:pk>/edit/", self.edit_view, name="edit"),
            path("<int:pk>/delete/", self.delete_view, name="delete"),
        ]

        if self.inspect_view_enabled:
            urlpatterns += [
                path("<int:pk>/", self.inspect_view, name="inspect"),
            ]

        return urlpatterns
