from django import forms

from wagtail.admin.panels import FieldPanel, MultiFieldPanel

# PAGES
# ------------------------------------------------------------------------------


def set_models_panels():
    from locolys.models import SiteSettings
    from locolys.models.newsletter import Newsletter
    from locolys.models.pages import (
        AbstractBasePage,
        ContentPage,
        HomePage,
        PostIndexPage,
        PostPage,
    )

    SiteSettings.panels = [
        MultiFieldPanel(
            [
                FieldPanel("facebook_url"),
                FieldPanel("instagram_url"),
            ],
            heading="Liens vers les réseaux",
        )
    ]

    Newsletter.panels = [
        FieldPanel(
            "name",
            classname="title",
            widget=forms.TextInput(
                attrs={"placeholder": "Nom de l'infolettre"}
            ),
        ),
        FieldPanel("description"),
    ]

    AbstractBasePage.promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("slug"),
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
            ],
            heading="Pour les moteurs de recherche",
        ),
        MultiFieldPanel(
            [
                FieldPanel("show_in_menus"),
                FieldPanel("navigation_menu"),
            ],
            heading="Pour les menus du site",
        ),
    ]

    HomePage.content_panels = [
        FieldPanel(
            "title",
            classname="title",
            widget=forms.TextInput(attrs={"placeholder": "Titre de la page"}),
        ),
        MultiFieldPanel(
            [
                FieldPanel("header_background_image"),
                FieldPanel("header_content"),
            ],
            heading="En-tête",
        ),
        MultiFieldPanel(
            [
                FieldPanel("last_posts_title"),
                FieldPanel("last_posts_limit"),
            ],
            heading="Derniers articles",
        ),
        FieldPanel("presentation"),
        MultiFieldPanel(
            [
                FieldPanel("block_1_background_image"),
                FieldPanel("block_1_content"),
            ],
            heading="Bloc de gauche",
        ),
        MultiFieldPanel(
            [
                FieldPanel("block_2_background_image"),
                FieldPanel("block_2_content"),
            ],
            heading="Bloc de droite",
        ),
    ]

    ContentPage.content_panels = [
        FieldPanel(
            "title",
            classname="title",
            widget=forms.TextInput(attrs={"placeholder": "Titre de la page"}),
        ),
        MultiFieldPanel(
            [
                FieldPanel("show_header"),
                FieldPanel(
                    "introduction",
                    widget=forms.Textarea(attrs={"rows": 2}),
                ),
            ],
            heading="En-tête",
        ),
        FieldPanel("body"),
    ]

    PostPage.promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("slug"),
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
            ],
            heading="Pour les moteurs de recherche",
        ),
        FieldPanel("date_published"),
    ]
    PostPage.content_panels = [
        FieldPanel(
            "title",
            classname="title",
            widget=forms.TextInput(attrs={"placeholder": "Titre de l'article"}),
        ),
        FieldPanel("body"),
    ]

    PostIndexPage.content_panels = [
        FieldPanel(
            "title",
            classname="title",
            widget=forms.TextInput(attrs={"placeholder": "Titre de la page"}),
        ),
    ]
