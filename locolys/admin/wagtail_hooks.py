from django.contrib.auth.models import Permission
from django.templatetags.static import static
from django.urls import path, reverse
from django.utils.html import format_html

from wagtail import hooks

from .menu import PermissionMenuItem
from .views.newsletter import (
    NewsletterSubscriptionsReportView,
    NewsletterViewSet,
)


@hooks.register("register_admin_menu_item")
def register_newsletter_menu():
    return PermissionMenuItem(
        "Infolettres",
        reverse("admin_newsletter:index"),
        NewsletterViewSet.permission_policy,
        icon_name=NewsletterViewSet.icon,
        order=500,
    )


@hooks.register("register_reports_menu_item")
def register_newsletter_subscriptions_report_menu_item():
    return PermissionMenuItem(
        "Abonnements aux infolettres",
        reverse("newsletter_subscriptions_report"),
        NewsletterSubscriptionsReportView.permission_policy,
        permissions=[NewsletterSubscriptionsReportView.permission_required],
        icon_name=NewsletterSubscriptionsReportView.header_icon,
        order=100,
    )


@hooks.register("register_admin_viewset")
def register_newsletter_viewset():
    return NewsletterViewSet("admin_newsletter", url_prefix="newsletter")


@hooks.register("register_admin_urls")
def register_newsletter_subscriptions_report_url():
    return [
        path(
            "reports/newsletter/",
            NewsletterSubscriptionsReportView.as_view(),
            name="newsletter_subscriptions_report",
        ),
    ]


@hooks.register("register_permissions")
def register_permissions():
    return Permission.objects.filter(
        codename__in=[
            "add_newsletter",
            "change_newsletter",
            "delete_newsletter",
            "view_newslettersubscription",
        ],
    )


@hooks.register("register_icons")
def register_icons(icons):
    for icon in ["button", "heading", "newspaper", "text-center"]:
        icons.append("icons/{}.svg".format(icon))
    return icons


@hooks.register("insert_global_admin_css")
def insert_global_admin_css():
    return format_html(
        '<link rel="stylesheet" href="{}">',
        static("admin/main.css"),
    )
