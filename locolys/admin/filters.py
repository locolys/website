from django import forms

from wagtail.admin.filters import WagtailFilterSet

import django_filters
from django_filters.widgets import SuffixedMultiWidget

from locolys.models import Newsletter, NewsletterSubscription


class IntegerRangeWidget(SuffixedMultiWidget):
    template_name = "admin/widgets/integerrange_input.html"
    suffixes = ["min", "max"]

    def __init__(self, attrs=None):
        widgets = (
            forms.NumberInput(attrs={"placeholder": "Minimum"}),
            forms.NumberInput(attrs={"placeholder": "Maximum"}),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.start, value.stop]
        return [None, None]


class NewsletterSubscriptionsFilterSet(WagtailFilterSet):
    class Meta:
        model = NewsletterSubscription
        fields = ["newsletters"]

    # Filtres

    newsletters = django_filters.ModelMultipleChoiceFilter(
        queryset=Newsletter.objects.all(),
        null_label="Aucune",
    )
