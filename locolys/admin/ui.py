from django.contrib.admin.utils import quote
from django.urls import reverse

from wagtail.admin.ui.tables import Column, Table
from wagtail.coreutils import multigetattr


class ActionColumn(Column):
    cell_template_name = "wagtailadmin/tables/action_cell.html"

    def __init__(
        self,
        name,
        title,
        url_name,
        icon_name,
        link_classname="button button--icon text-replace",
        link_attrs=None,
        id_accessor="pk",
        label="",
        classname=None,
        width=40,
    ):
        super().__init__(name, label=label, classname=classname, width=width)
        self.title = title
        self.url_name = url_name
        self.icon_name = icon_name
        self.link_attrs = link_attrs or {}
        self.link_classname = link_classname
        self.id_accessor = id_accessor

    def get_value(self, instance):
        return self.title

    def get_cell_context_data(self, instance, parent_context):
        context = super().get_cell_context_data(instance, parent_context)
        context.update(
            {
                "icon_name": self.icon_name,
                "link_attrs": self.get_link_attrs(instance, parent_context),
            }
        )
        context["link_attrs"]["href"] = self.get_link_url(
            instance, parent_context
        )
        if self.link_classname is not None:
            context["link_attrs"]["class"] = self.link_classname
        return context

    def get_link_attrs(self, instance, parent_context):
        return self.link_attrs.copy()

    def get_link_url(self, instance, parent_context):
        id = multigetattr(instance, self.id_accessor)
        return reverse(self.url_name, args=(quote(id),))


class FieldColumn(Column):
    class Cell:
        def __init__(self, column, form):
            self.column = column
            self.form = form

        def render_html(self, parent_context):
            return self.column.render_cell_html(self.form, parent_context)

    cell_template_name = "wagtailadmin/tables/field_cell.html"

    def get_field(self, form):
        return form[self.accessor]

    def get_cell_context_data(self, form, parent_context):
        return {
            "column": self,
            "field": self.get_field(form),
            "request": parent_context.get("request"),
        }

    def render_cell_html(self, form, parent_context):
        context = self.get_cell_context_data(form, parent_context)
        return self.cell_template.render(context)

    def get_cell(self, form):
        return FieldColumn.Cell(self, form)


class FormSetTable(Table):
    class Row(Table.Row):
        def __init__(self, table, form):
            self.table = table
            self.columns = table.columns
            self.form = form
            self.instance = form.instance

        def __getitem__(self, key):
            return self.get_cell(self.columns[key])

        def __iter__(self):
            for name in self.columns:
                yield name

        def __repr__(self):
            return repr([self.get_cell(col) for col in self.columns.values()])

        def get_cell(self, column):
            if isinstance(column, FieldColumn):
                return column.get_cell(self.form)
            return column.get_cell(self.instance)

    template_name = "wagtailadmin/tables/formset_table.html"
    classname = "listing listing--formset"

    @property
    def formset(self):
        return self.data

    @property
    def rows(self):
        for form in self.data:
            yield FormSetTable.Row(self, form)

    def get_context_data(self, parent_context):
        context = super().get_context_data(parent_context)
        context["csrf_token"] = parent_context.get("csrf_token")
        context["formset"] = self.formset
        return context
