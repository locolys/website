from wagtail.permissions import ModelPermissionPolicy

from locolys.models import Newsletter, NewsletterSubscription


class OwnerModelPermissionPolicy(ModelPermissionPolicy):
    def user_has_permission_for_instance(self, user, action, instance):
        # Donne tous les droits sur l'instance de l'utilisateur
        if instance.user == user:
            return True
        return super().user_has_permission_for_instance(user, action, instance)


newsletter_permission_policy = ModelPermissionPolicy(Newsletter)
newsletter_subscription_permission_policy = ModelPermissionPolicy(
    NewsletterSubscription
)
