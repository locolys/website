from numbers import Number

from django.forms import widgets


class NumericInput(widgets.TextInput):
    def __init__(self, attrs=None, format=None):
        super().__init__(attrs)
        self.format = format
        self.attrs.setdefault("inputmode", "numeric")

    def format_value(self, value):
        if value == "" or value is None:
            return None
        if self.format and isinstance(value, Number):
            return "%{}".format(self.format) % float(value)
        return str(value)
