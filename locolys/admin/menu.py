from wagtail.admin.menu import MenuItem


class PermissionMenuItem(MenuItem):
    def __init__(
        self, label, url, permission_policy, permissions=None, **kwargs
    ):
        self.permission_policy = permission_policy
        self.permissions = permissions
        super().__init__(label, url, **kwargs)

    def is_shown(self, request):
        return self.permission_policy.user_has_any_permission(
            request.user, self.permissions or ["add", "change", "delete"]
        )
