from wagtail.models import Collection, GroupCollectionPermission

import factory
from factory.django import DjangoModelFactory


class GroupCollectionPermissionFactory(DjangoModelFactory):
    class Meta:
        model = GroupCollectionPermission

    @factory.lazy_attribute
    def collection(self):
        return Collection.get_first_root_node()
