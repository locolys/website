from html import escape

from django.urls import reverse

from locolys.models import Newsletter
from locolys.tests.factories import (
    NewsletterFactory,
    NewsletterSubscriptionFactory,
)

from .utils import AdminViewMixin, AdminViewSetMixin


class TestNewsletterViews(AdminViewSetMixin):
    namespace = "admin_newsletter"

    def test_index(self):
        NewsletterFactory.reset_sequence()
        newsletters = NewsletterFactory.create_batch(2)
        NewsletterSubscriptionFactory(newsletters=[newsletters[0]])

        html = self.get(self.index_url, status=200).html
        assert html.find("a", href=self.add_url).text == (
            "Ajouter une infolettre"
        )
        rows = html.select(".listing tbody tr")
        assert len(rows) == 2
        assert rows[0].select("td")[0].text.strip() == newsletters[0].name
        assert rows[0].select("td")[1].text.strip() == "1"
        assert rows[1].select("td")[0].text.strip() == newsletters[1].name
        assert rows[1].select("td")[1].text.strip() == "0"

    def test_add(self):
        response = self.get(self.add_url)
        assert "Ajouter une infolettre" in response
        form = response.forms[0]
        assert form.action == self.add_url

        form["name"] = "Blabla"
        form["description"] = "Une description de cette lettre"
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert escape("Infolettre « Blabla » ajoutée.") in response.follow()

        Newsletter.objects.get(name="Blabla")

    def test_edit(self):
        newsletter = NewsletterFactory()
        url = self.get_edit_url(newsletter)

        form = self.get(url).forms[0]
        form["name"] = "Bla"
        response = form.submit(status=302)
        assert response["location"] == self.index_url
        assert escape("Infolettre « Bla » mise à jour.") in response.follow()

        newsletter.refresh_from_db()
        newsletter.name == "Bla"

    def test_delete(self):
        newsletter = NewsletterFactory()
        url = self.get_delete_url(newsletter)

        form = self.get(url, status=200).forms[0]
        assert form.action == url

        form.submit().follow(status=200)
        assert not Newsletter.objects.filter(pk=newsletter.pk).exists()


class TestNewsletterSubscriptionsReportView(AdminViewMixin):
    url = reverse("newsletter_subscriptions_report")

    def test_list(self):
        sub1 = NewsletterSubscriptionFactory()
        sub2 = NewsletterSubscriptionFactory()
        NewsletterSubscriptionFactory(is_active=False)

        html = self.get().html
        rows = html.select(".listing tbody tr")
        assert len(rows) == 2
        assert rows[0].select("td")[0].text.strip() == sub2.email
        assert rows[1].select("td")[0].text.strip() == sub1.email

    def test_filter(self):
        newsletters = NewsletterFactory.create_batch(2)
        sub = NewsletterSubscriptionFactory(newsletters=[newsletters[0]])
        NewsletterSubscriptionFactory.create_batch(2)

        filter_form = self.get().forms[0]
        filter_form["newsletters"] = [newsletters[0].pk]
        html = filter_form.submit().html
        rows = html.select(".listing tbody tr")
        assert len(rows) == 1
        assert rows[0].select("td")[0].text.strip() == sub.email

        filter_form["newsletters"] = [newsletters[1].pk]
        html = filter_form.submit().html
        rows = html.select(".listing tbody tr")
        assert len(rows) == 0

    def test_unauthorized(self, editor_user):
        self.get(user=editor_user, status=302)
