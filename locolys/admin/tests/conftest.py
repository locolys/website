from django.contrib.auth.models import Permission

import pytest

from locolys.tests.factories import GroupFactory, UserFactory

from .factories import GroupCollectionPermissionFactory


@pytest.fixture
def editor_group():
    group = GroupFactory(name="Editor", permissions__codenames=["access_admin"])
    for codename in ["add_page", "change_page"]:
        GroupCollectionPermissionFactory(
            group=group,
            permission=Permission.objects.get(codename=codename),
        )
    return group


@pytest.fixture
def editor_user(editor_group):
    return UserFactory(email="manager@example.org", groups=[editor_group])
