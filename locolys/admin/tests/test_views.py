from django.contrib.auth.models import Permission
from django.urls import reverse

from .utils import AdminViewMixin


class TestWagtailGroupPermissionsView(AdminViewMixin):
    add_url = reverse("wagtailusers_groups:add")

    def test_custom_permissions(self):
        html = self.get(self.add_url).html

        row = html.find("td", string="Abonnement aux infolettres").parent
        permissions = row.select('input[name="permissions"]')
        assert len(permissions) == 1
        assert int(permissions[0].attrs["value"]) == (
            Permission.objects.get(codename="view_newslettersubscription").pk
        )

        row = html.find("td", string="Infolettre").parent
        permissions = row.select('input[name="permissions"]')
        assert len(permissions) == 3
