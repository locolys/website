from django.urls import reverse
from django.utils.functional import cached_property

import pytest

from locolys.tests.utils import ViewMixin


class AdminViewMixin(ViewMixin):
    """
    Étend `ViewMixin` en positionnant un compte administrateur comme
    utilisateur à utiliser dans les requêtes.
    """

    @pytest.fixture(autouse=True)
    def setup_user(self, admin_user):
        self.user = admin_user


class AdminViewSetMixin(AdminViewMixin):
    """
    Étend `AdminViewMixin` en fournissant un accès rapide aux URLs de base
    pour `wagtail.admin.viewset`.
    """

    namespace = None

    @cached_property
    def index_url(self):
        return reverse(self.get_url_name("index"))

    @cached_property
    def add_url(self):
        return reverse(self.get_url_name("add"))

    def get_edit_url(self, obj):
        return reverse(self.get_url_name("edit"), args=(obj.pk,))

    def get_delete_url(self, obj):
        return reverse(self.get_url_name("delete"), args=(obj.pk,))

    def get_url_name(self, view_name):
        return "{}:{}".format(self.namespace, view_name)
