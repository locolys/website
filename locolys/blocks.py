from wagtail.blocks import (
    CharBlock,
    ChoiceBlock,
    PageChooserBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from wagtail_cblocks import blocks as cblocks
from wagtail_cblocks.blocks import CSSClassMixin, Style, StylizedStructBlock

from .forms.newsletter import NewsletterSubscribeBlockForm

BUTTON_STYLES = [
    Style("primary", "Principal (cyan)", "btn-primary"),
    Style("secondary", "Secondaire (jaune)", "btn-secondary"),
]
BUTTON_DEFAULT_STYLE = "primary"

BUTTON_SIZES = [
    ("sm", "Petit"),
    ("lg", "Grand"),
]

HERO_STYLES = [
    Style("primary", "Principal (cyan)", "bg-primary text-white"),
    Style("secondary", "Secondaire (jaune)", "bg-secondary text-gray-800"),
    Style("gray", "Gris", "bg-gray-300"),
    Style("white", "Blanc", "bg-white"),
]
HERO_DEFAULT_STYLE = "primary"

TEXT_ALIGN = {
    "start": "text-left",
    "center": "text-center",
    "end": "text-right",
}

# BLOCS DE CONTENU
# ------------------------------------------------------------------------------


class HeadingBlock(cblocks.HeadingBlock):
    class Meta:
        icon = "heading"
        template = "blocks/heading_block.html"


class EpigrahBlock(TextBlock):
    class Meta:
        label = "Épigraphe"
        icon = "text-center"
        template = "blocks/epigraph_block.html"


class ParagraphBlock(cblocks.ParagraphBlock):
    class Meta:
        template = "blocks/paragraph_block.html"


class SizedButtonBlock(cblocks.ButtonBlock):
    size = ChoiceBlock(choices=BUTTON_SIZES, label="Taille", required=False)

    styles = BUTTON_STYLES

    class Meta:
        icon = "button"
        template = "blocks/sized_button_block.html"
        default_style = BUTTON_DEFAULT_STYLE

    def get_css_classes(self, value):
        css_classes = super().get_css_classes(value)
        size = value.get("size", None)
        if size:
            css_classes.append("btn-%s" % size)
        return css_classes


class ImageBlock(cblocks.ImageBlock):
    class Meta:
        template = "blocks/image_block.html"


class MediaObjectBlock(StructBlock):
    image = ImageChooserBlock()
    body = StreamBlock(
        [
            ("heading_block", HeadingBlock()),
            ("epigraph_block", EpigrahBlock()),
            ("paragraph_block", ParagraphBlock()),
            ("button_block", SizedButtonBlock()),
        ],
        label="Contenu",
    )

    class Meta:
        label = "Objet média"
        icon = "image"
        template = "blocks/media_object_block.html"


class NewsletterSubscribeBlock(StructBlock):
    title = CharBlock(label="Titre")
    description = TextBlock(label="Description")

    class Meta:
        label = "Lettre d’information"
        icon = "newspaper"
        template = "blocks/newsletter_subscribe_block.html"

    def get_context(self, value, **kwargs):
        context = super().get_context(value, **kwargs)
        context["form"] = NewsletterSubscribeBlockForm()
        return context


# BLOCS STRUCTURELS
# ------------------------------------------------------------------------------


class ContentBlock(StreamBlock):
    heading_block = HeadingBlock()
    epigraph_block = EpigrahBlock()
    paragraph_block = ParagraphBlock()
    button_block = SizedButtonBlock()
    image_block = ImageBlock()


class ColumnsBlock(CSSClassMixin, cblocks.ColumnsBlock):
    class Meta:
        template = "blocks/hero_columns_block.html"
        column_block = ContentBlock()

    def get_css_classes(self, value):
        return TEXT_ALIGN.get(value.get("horizontal_align", None), "")


class SectionBlock(ContentBlock):
    media_object_block = MediaObjectBlock()
    columns_block = ColumnsBlock()

    class Meta:
        label = "Section de page"
        icon = "doc-full"
        template = "blocks/section_block.html"


class HeroBlock(StylizedStructBlock):
    title = CharBlock(label="Titre", required=False)
    blocks = ContentBlock(
        local_blocks=[
            (
                "columns_block",
                ColumnsBlock(template="blocks/hero_columns_block.html"),
            )
        ],
        label="Contenu",
    )

    styles = HERO_STYLES

    class Meta:
        label = "Bandeau coloré"
        template = "blocks/hero_block.html"
        default_style = HERO_DEFAULT_STYLE


class BodyBlock(StreamBlock):
    section_block = SectionBlock()
    hero_block = HeroBlock()
    newsletter_block = NewsletterSubscribeBlock()

    class Meta:
        collapsed = True


# BLOCS SPECIFIQUES
# ------------------------------------------------------------------------------


class HomeButtonBlock(StructBlock):
    title = CharBlock(label="Titre")
    subtitle = CharBlock(label="Sous-titre", required=False)
    page_link = PageChooserBlock(label="Lien")

    class Meta:
        label = "Bouton"
        template = "blocks/home_button_block.html"
