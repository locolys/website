from django.contrib.auth.models import Group, Permission

import factory
from factory.django import DjangoModelFactory

from .. import models


class UserFactory(DjangoModelFactory):
    class Meta:
        model = models.User

    email = factory.Sequence(lambda x: "user{0}@example.org".format(x))
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    address = "3 rue des marguerites"
    city = "Libreville"
    phone = "+33123456789"

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Passe par UserManager afin de définir correctement le mot de passe
        return cls._get_manager(model_class).create_user(*args, **kwargs)

    @factory.post_generation
    def groups(obj, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for group in extracted:
                obj.groups.add(group)

    @factory.post_generation
    def user_permissions(obj, create, extracted, codenames=None, **kwargs):
        if not create:
            return
        if codenames:
            for perm in Permission.objects.filter(codename__in=codenames):
                obj.user_permissions.add(perm)
        if extracted:
            for perm in extracted:
                obj.user_permissions.add(perm)


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda x: "Groupe #{0}".format(x))

    @factory.post_generation
    def permissions(obj, create, extracted, codenames=None, **kwargs):
        if not create:
            return
        if codenames:
            for perm in Permission.objects.filter(codename__in=codenames):
                obj.permissions.add(perm)
        if extracted:
            for perm in extracted:
                obj.permissions.add(perm)


class NewsletterFactory(DjangoModelFactory):
    class Meta:
        model = models.Newsletter

    name = factory.Sequence(lambda x: "Infolettre #{0}".format(x))
    description = "Une description de cette infolettre."


class NewsletterSubscriptionFactory(DjangoModelFactory):
    class Meta:
        model = models.NewsletterSubscription

    email = factory.Sequence(lambda x: "user{0}@example.org".format(x))
    is_active = True

    @factory.post_generation
    def newsletters(obj, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for newsletter in extracted:
                obj.newsletters.add(newsletter)
