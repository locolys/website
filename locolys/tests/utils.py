import pytest


class ViewMixin:
    """
    Mixin ajoutant des facilitées pour faire des requêtes avec Webtest, à
    appliquer aux classes définissant des tests sur les vues.
    """

    #: L'URL à utiliser par défaut pour les requêtes.
    url = None

    #: L'utilisateur à positionner par défaut dans les requêtes.
    user = None

    #: Détermine s'il faut activer la protection CSRF.
    csrf_checks = True

    @pytest.fixture(autouse=True)
    def setup_django_app(self, django_app_factory):
        self.django_app = django_app_factory(csrf_checks=self.csrf_checks)

    def _query(self, method, url=None, **kwargs):
        """
        Exécute la requête de type `method` vers `url`. Pour les autres
        arguments disponibles, voir `webtest.app.TestApp.get`.
        """
        kwargs.setdefault("user", self.user)
        return getattr(self.django_app, method)(url or self.url, **kwargs)

    def get(self, url=None, **kwargs):
        return self._query("get", url, **kwargs)

    def post(self, url=None, **kwargs):
        return self._query("post", url, **kwargs)
