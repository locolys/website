import re

from django.conf import settings
from django.contrib.messages import INFO as INFO_LEVEL
from django.contrib.messages import SUCCESS as SUCCESS_LEVEL
from django.urls import reverse

from pytest_django.asserts import assertRedirects

from .factories import (
    NewsletterFactory,
    NewsletterSubscriptionFactory,
    UserFactory,
)
from .utils import ViewMixin

LOGIN_URL = reverse("account:login")
URL_RE = re.compile(r"http(|s)://[^/]*/([^ ]*/)$", re.MULTILINE)


class TestLoginLogout(ViewMixin):
    url = LOGIN_URL

    def test_login(self, test_user):
        form = self.get().form
        form["username"] = test_user.email
        form["password"] = "password"
        response = form.submit()
        assertRedirects(response, settings.HOME_URL)

    def test_login_invalid(self, test_user):
        form = self.get().form
        form["username"] = test_user.email
        form["password"] = "pouet"
        response = form.submit()
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["__all__"][0] == (
            "L’adresse mail et le mot de passe ne correspondent pas, "
            "veuillez réessayer."
        )

    def test_login_inactive(self, test_user):
        test_user.is_active = False
        test_user.save()

        form = self.get().form
        form["username"] = test_user.email
        form["password"] = "password"
        response = form.submit()
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["__all__"][0] == (
            "L’adresse mail et le mot de passe ne correspondent pas, "
            "veuillez réessayer."
        )

    def test_logout(self, test_user):
        response = self.get(
            reverse("account:profile_update"), user=test_user, status=200
        )
        response = response.forms["logout-form"].submit()
        assertRedirects(response, self.url)
        # un message indique la déconnexion
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL


class TestPasswordReset(ViewMixin):
    url = reverse("account:password_reset")

    def test_workflow(self, mailoutbox):
        user = UserFactory(email="iforgot@example.org", password="")

        # 1. On demande un lien de réinitialisation pour le compte
        form = self.get().form
        form["email"] = user.email
        response = form.submit()
        # … on est redirigé vers la page de connexion
        assertRedirects(response, LOGIN_URL)
        # … avec un message d'information
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # … un courriel a été envoyé
        assert len(mailoutbox) == 1
        # … avec le lien de réinitialisation.
        confirm_url = URL_RE.search(mailoutbox[0].body).group()
        assert confirm_url

        # 2. On suit le lien et on confirme la réinitialisation
        form = self.get(confirm_url).follow().form
        form["new_password1"] = "SomePass2"
        form["new_password2"] = "SomePass2"
        response = form.submit()
        # … on est redirigé vers la page de connexion
        assertRedirects(response, LOGIN_URL)
        # … avec un message d'information
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        # … on peut se connecter avec le nouveau mot de passe.
        assert response.client.login(username=user.email, password="SomePass2")

        # 3. Le lien n'est désormais plus valide
        response = self.get(confirm_url)
        assert not response.forms

    def test_email_unknown(self, mailoutbox):
        # On demande un lien de réinitialisation pour une adresse inexistante
        form = self.get().form
        form["email"] = "idontexist@no.lan"
        response = form.submit()
        # … on est redirigé vers la page de connexion
        assertRedirects(response, LOGIN_URL)
        # … avec un message d'information
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # … mais aucun courriel n'est envoyé.
        assert len(mailoutbox) == 0


class TestProfileUpdate(ViewMixin):
    url = reverse("account:profile_update")

    def test_get(self, test_user):
        response = self.get(user=test_user, status=200)
        assert response.forms[1].action == self.url
        # L'adresse mail se change via un autre formulaire
        assert "email" not in response.forms[1].fields
        assert (
            response.html.select_one("#userEmail").attrs["value"]
            == test_user.email
        )

    def test_update(self, test_user):
        form = self.get(user=test_user, status=200).forms[1]

        form["city"] = ""
        form["phone"] = "0123456789"
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert "city" in response.context["form"].errors

        form = response.forms[1]
        form["city"] = "testville"
        response = form.submit()
        assertRedirects(response, self.url)
        response.follow()

        test_user.refresh_from_db()
        assert test_user.city == "testville"
        assert str(test_user.phone) == "+33123456789"

    def test_nav(self, test_user):
        current_links = self.get(user=test_user).html.select(
            '#submenu [aria-current="page"]'
        )

        assert len(current_links) == 1
        assert current_links[0].text.strip() == "Mon compte"

    def test_anonymous(self):
        assertRedirects(self.get(), "{}?next={}".format(LOGIN_URL, self.url))


class TestPasswordChange(ViewMixin):
    url = reverse("account:password_change")

    def test_get(self, test_user):
        html = self.get(user=test_user, status=200).html
        assert (
            html.select_one("title").text.strip()
            == "Changer mon mot de passe - LocoLys"
        )
        assert html.select_one("header h2").text.strip() == "Mon compte"

    def test_update(self, test_user):
        form = self.get(user=test_user, status=200).forms[1]
        assert form.action == self.url

        # On change de mot de passe
        form["old_password"] = "password"
        form["new_password1"] = "SomePass2"
        form["new_password2"] = "SomePass2"
        response = form.submit()
        # … on est redirigé vers la page « Mon compte »
        assertRedirects(response, reverse("account:profile_update"))
        # … avec un message d'information
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        # … on peut se connecter avec le nouveau mot de passe.
        assert response.client.login(
            username=test_user.email, password="SomePass2"
        )

    def test_nav(self, test_user):
        current_links = self.get(user=test_user).html.select(
            '#submenu [aria-current="page"]'
        )

        assert len(current_links) == 1
        assert current_links[0].text.strip() == "Mon compte"

    def test_unauthorized(self):
        assertRedirects(self.get(), "{}?next={}".format(LOGIN_URL, self.url))


class TestEmailChange(ViewMixin):
    url = reverse("account:email_change")

    def test_get(self, test_user):
        html = self.get(user=test_user, status=200).html
        assert (
            html.select_one("title").text.strip()
            == "Changer mon adresse mail - LocoLys"
        )
        assert html.select_one("header h2").text.strip() == "Mon compte"

    def test_update(self, test_user):
        form = self.get(user=test_user, status=200).forms[1]
        assert form.action == self.url

        # On change d'adresse mail
        form["email"] = "autre@example.org"
        response = form.submit()
        # … on est redirigé vers la page « Mon compte »
        assertRedirects(response, reverse("account:profile_update"))
        # … avec un message d'information
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        # … l'adresse mail a été changée.
        test_user.refresh_from_db()
        assert test_user.email == "autre@example.org"

    def test_unique_email(self, test_user):
        form = self.get(user=UserFactory(), status=200).forms[1]
        form["email"] = "User@example.org"
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["email"][0] == (
            "Un compte avec cette adresse mail existe déjà."
        )

    def test_nav(self, test_user):
        current_links = self.get(user=test_user).html.select(
            '#submenu [aria-current="page"]'
        )

        assert len(current_links) == 1
        assert current_links[0].text.strip() == "Mon compte"

    def test_unauthorized(self):
        assertRedirects(self.get(), "{}?next={}".format(LOGIN_URL, self.url))


class TestSubscriptionsUpdate(ViewMixin):
    url = reverse("account:subscriptions_update")

    def test_get(self, test_user):
        html = self.get(user=test_user, status=200).html
        assert html.select_one("header h2").text.strip() == "Mes abonnements"

    def test_update(self, test_user):
        newsletters = NewsletterFactory.create_batch(2)

        form = self.get(user=test_user).forms[1]
        assert len(form.fields["newsletters"]) == 2
        assert form.fields["newsletters"][0].checked is False
        assert form.fields["newsletters"][1].checked is False
        assert form["email"].value == test_user.email

        form.fields["newsletters"][0].checked = True
        form["email"] = "toto@example.lan"
        response = form.submit()
        assertRedirects(response, self.url)
        response = response.follow()

        test_user.refresh_from_db()
        test_user.newsletter_subscription.is_active is True
        test_user.newsletter_subscription.email == "toto@example.lan"
        test_user.newsletter_subscription.newsletters.get() == newsletters[0]

        form = response.forms[1]
        assert form.fields["newsletters"][0].checked is True
        assert form.fields["newsletters"][1].checked is False
        assert form["email"].value == "toto@example.lan"

    def test_update_error(self, test_user):
        form = self.get(user=test_user).forms[1]
        form["email"] = ""
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["email"][0] == "Champ requis."

        form = response.forms[1]
        form["email"] = "toto"
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["email"][0] == (
            "Veuillez saisir une adresse mail valide."
        )

    def test_link_to_user(self, test_user):
        subscription = NewsletterSubscriptionFactory(
            email=test_user.email, newsletters=[NewsletterFactory()]
        )

        form = self.get(user=test_user).forms[1]
        form["email"] = test_user.email
        form.submit(status=302)

        subscription.refresh_from_db()
        assert subscription.user == test_user

    def test_nav(self, test_user):
        current_links = self.get(user=test_user).html.select(
            '#submenu [aria-current="page"]'
        )

        assert len(current_links) == 1
        assert current_links[0].text.strip() == "Mes abonnements"

    def test_anonymous(self):
        assertRedirects(self.get(), "{}?next={}".format(LOGIN_URL, self.url))
