from django.contrib.messages import SUCCESS as SUCCESS_LEVEL
from django.urls import reverse

from pytest_django.asserts import assertRedirects

from ..models import NewsletterSubscription
from .factories import NewsletterFactory, NewsletterSubscriptionFactory
from .utils import ViewMixin


class TestSubscribe(ViewMixin):
    url = reverse("newsletter:subscribe")

    def test_authenticated(self, test_user):
        assertRedirects(
            self.get(user=test_user), reverse("account:subscriptions_update")
        )

    def test_create(self):
        newsletters = NewsletterFactory.create_batch(2)

        form = self.get().form
        form["email"] = "Test"
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert set(response.context["form"].errors.keys()) == {"email"}

        form = self.get().form
        form["email"] = "Test@example.org"
        response = form.submit(status=200)
        assert response.context["form"].is_valid() is False
        assert response.context["form"].errors["newsletters"][0] == (
            "Veuillez choisir au moins une infolettre."
        )

        form = response.form
        form["newsletters"] = [newsletters[0].pk]
        response = form.submit()
        assertRedirects(response, "http://testserver%s" % self.url)
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        assert messages[0].message == (
            "Vous êtes désormais abonné⋅e aux infolettres avec l’adresse "
            "mail <i>test@example.org</i>."
        )

        subscription = NewsletterSubscription.objects.get(
            email="test@example.org"
        )
        assert subscription.is_active is True
        assert list(subscription.newsletters.values_list("pk", flat=True)) == [
            newsletters[0].pk
        ]

    def test_update(self):
        newsletter = NewsletterFactory()
        subscription = NewsletterSubscriptionFactory(
            email="test@example.org", newsletters=[NewsletterFactory()]
        )

        form = self.get().form
        form["email"] = "Test@example.org"
        form["newsletters"] = [newsletter.pk]
        response = form.submit(status=302).follow()
        messages = list(response.context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        assert messages[0].message == (
            "Votre abonnement aux infolettres a été mis à jour."
        )

        subscription.refresh_from_db()
        assert list(subscription.newsletters.values_list("pk", flat=True)) == [
            newsletter.pk
        ]

    def test_delete(self):
        NewsletterSubscriptionFactory(
            email="test@example.org", newsletters=[NewsletterFactory()]
        )

        form = self.get().form
        form["email"] = "Test@example.org"
        form["newsletters"] = []
        response = form.submit(status=302).follow()
        messages = list(response.context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
        assert messages[0].message == (
            "Vous avez été désabonné⋅e des infolettres."
        )

        assert not NewsletterSubscription.objects.filter(
            email="test@example.org"
        ).exists()

    def test_redirect_unsafe(self):
        newsletter = NewsletterFactory()

        form = self.get().form
        form["email"] = "Test@example.org"
        form["newsletters"] = [newsletter.pk]
        response = form.submit(headers={"Referer": "http://example.org/"})
        assertRedirects(response, self.url)


class TestUnsubscribe(ViewMixin):
    url = reverse("newsletter:unsubscribe")

    def test_authenticated(self, test_user):
        assertRedirects(
            self.get(user=test_user), reverse("account:subscriptions_update")
        )

    def test_delete(self):
        NewsletterSubscriptionFactory(
            email="test@example.org", newsletters=[NewsletterFactory()]
        )

        form = self.get().form
        form["email"] = "Test@example.org"
        response = form.submit()
        assertRedirects(response, TestSubscribe.url)
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL

        assert not NewsletterSubscription.objects.filter(
            email="test@example.org"
        ).exists()

    def test_delete_unknown(self):
        form = self.get().form
        form["email"] = "test@example.org"
        response = form.submit()
        assertRedirects(response, TestSubscribe.url)
        messages = list(response.follow().context["messages"])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL
