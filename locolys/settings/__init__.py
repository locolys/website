import environ

"""The default environment to use."""
DEFAULT_ENVIRONMENT = "production"

"""The environment variables of the app instance."""
env = environ.Env()

"""Path to the package root - e.g. Django project."""
root_dir = environ.Path(__file__) - 2

"""Path to the base directory of the app instance."""
base_dir = env.path("BASE_DIR", default=str(root_dir - 1))

# Load config.env, OS environment variables will take precedence unless
# CONFIG_PRIORITY_TO_FILE is set to `True` - the default. This way, this
# configuration file will be read again when the application is reloaded.
env.read_env(
    base_dir("config.env"),
    overwrite=env.bool("CONFIG_PRIORITY_TO_FILE", default=True),
)

"""The Django settings module's name to use."""
DJANGO_SETTINGS_MODULE = env(
    "DJANGO_SETTINGS_MODULE",
    default="locolys.settings.{}".format(
        env("ENV", default=DEFAULT_ENVIRONMENT)
    ),
)
