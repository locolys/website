from django.utils.html import escape
from django.utils.safestring import mark_safe

from wagtail.images.formats import (
    Format,
    register_image_format,
    unregister_image_format,
)


class FigureFormat(Format):
    def __init__(self, name, label, with_caption=False):
        super().__init__(name, label, "", "width-500")
        self.with_caption = with_caption

    def image_to_html(self, image, alt_text, extra_attributes=None):
        img = super().image_to_html(image, alt_text, extra_attributes)
        if self.with_caption:
            img += "<figcaption>{}</figcaption>".format(escape(alt_text))
        return mark_safe("<figure>{}</figure>".format(img))


unregister_image_format("left")
unregister_image_format("right")
unregister_image_format("fullwidth")

register_image_format(FigureFormat("fullwidth", "Sans légende"))
register_image_format(FigureFormat("fullwidth-caption", "Avec légende", True))
