from dataclasses import dataclass


@dataclass
class LinkMenuItem:
    name: str
    label: str
    url: str
    icon_name: str = ""
