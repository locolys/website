def normalize_email(email):
    """
    Normalise l'adresse mail en supprimant les espaces et en la mettant
    entièrement en minuscules.
    """
    return email.strip().lower() if email else ""
