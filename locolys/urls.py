from django.conf import settings
from django.urls import include, path

from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls

from .views import account as account_views
from .views import newsletter as newsletter_views

account_patterns = [
    path("login/", account_views.LoginView.as_view(), name="login"),
    path("logout/", account_views.LogoutView.as_view(), name="logout"),
    path(
        "password/reset/",
        account_views.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "password/reset/<uidb64>/<token>/",
        account_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "password/change/",
        account_views.PasswordChangeView.as_view(),
        name="password_change",
    ),
    path(
        "profile/",
        account_views.ProfileUpdateView.as_view(),
        name="profile_update",
    ),
    path(
        "email/",
        account_views.EmailChangeView.as_view(),
        name="email_change",
    ),
    path(
        "subscriptions/",
        account_views.SubscriptionsUpdateView.as_view(),
        name="subscriptions_update",
    ),
]

newsletter_patterns = [
    path(
        "subscribe/",
        newsletter_views.SubscribeView.as_view(),
        name="subscribe",
    ),
    path(
        "unsubscribe/",
        newsletter_views.UnsubscribeView.as_view(),
        name="unsubscribe",
    ),
]

urlpatterns = [
    path("admin/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("account/", include((account_patterns, "account"))),
    path("newsletter/", include((newsletter_patterns, "newsletter"))),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.views import defaults as default_views

    # Sers les pages d'erreurs et les médias en développement
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns.insert(0, path("__debug__/", include(debug_toolbar.urls)))

# Laisse Wagtail gérer le reste à la racine
urlpatterns += [path("", include(wagtail_urls))]
