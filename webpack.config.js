/* eslint-env node */
const path = require('path');

const CopyPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');

// Paths to entries which will be bundled
const ENTRIES = {
  main: ['./assets/js/main.js', './assets/css/main.css'],
};

// Path to other assets wich will be copied with copy-webpack-plugin
// See: https://www.npmjs.com/package/copy-webpack-plugin
const COPY_PATTERNS = [
  { from: 'assets/img', to: 'img' },
  { from: 'assets/favicon', to: '' },
];

// SVG Sprite's generation for the defined icons
// See: https://www.npmjs.com/package/svg-spritemap-webpack-plugin
const ICONS_PATTERN = 'assets/icons/*.svg';
const ICONS_SPRITE_FILENAME = 'icons-sprite.svg';

// Path to the build output, which can safely be cleaned
const BUILD_PATH = 'locolys/static';

// Host and port the development server will listen on
const SERVER_HOST = 'localhost';
const SERVER_PORT = 'auto';

// Proxy target of the development server
const PROXY_TARGET = 'http://127.0.0.1:8000';

// Return Webpack configuration depending on the mode.
module.exports = function (_, argv) {
  const isProduction = argv.mode === 'production';

  const config = {
    entry: ENTRIES,
    output: {
      clean: true,
      filename: '[name].js',
      chunkFilename: '[name].[contenthash].js',
      path: path.resolve(__dirname, BUILD_PATH),
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: { loader: 'babel-loader' },
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '',
              },
            },
            { loader: 'css-loader' },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [
                    'postcss-import',
                    'tailwindcss/nesting',
                    'tailwindcss',
                    'autoprefixer',
                  ],
                },
              },
            },
          ],
        },
        {
          test: /\.(png|jpe?g|gif|svg)$/,
          type: 'asset/resource',
          generator: {
            filename: 'img/[name][ext]',
          },
        },
        {
          test: /\.(woff2?|ttf|otf|eot)$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name][ext]',
          },
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[name].[contenthash].css',
      }),
    ],
    stats: true,
  };

  if (COPY_PATTERNS.length) {
    config.plugins.push(
      new CopyPlugin({
        patterns: COPY_PATTERNS,
      }),
    );
  }

  if (ICONS_PATTERN) {
    config.plugins.push(
      new SVGSpritemapPlugin(ICONS_PATTERN, {
        input: {
          options: {
            realpath: true,
          },
        },
        output: {
          filename: ICONS_SPRITE_FILENAME,
        },
        sprite: {
          prefix: '',
          generate: { title: false },
        },
      }),
    );
  }

  if (isProduction) {
    config.mode = 'production';
    config.devtool = 'source-map';

    config.optimization = {
      minimizer: [
        new TerserJSPlugin({ extractComments: false }),
        new CssMinimizerPlugin(),
      ],
    };
  } else {
    config.mode = 'development';
    config.devtool = 'cheap-module-source-map';
    config.watchOptions = {
      aggregateTimeout: 1000,
    };

    config.devServer = {
      client: {
        overlay: {
          errors: true,
          warnings: false,
        },
        reconnect: false,
      },
      devMiddleware: {
        index: false,
        publicPath: '/static',
        writeToDisk: true,
      },
      host: SERVER_HOST,
      port: SERVER_PORT,
      proxy: {
        context: () => true,
        target: PROXY_TARGET,
      },
    };
  }

  return config;
};
