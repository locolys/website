# -*- mode: makefile-gmake -*-
## Définition des variables
# Le nom de l'exécutable Python à utiliser (ex. python)
PYTHON_BASENAME := python3
# Configuration de l'environnement virtuel
VENV_DIR := venv
VENV_OPT := --system-site-packages

# Définis les chemins des exécutables
PYTHON := $(VENV_DIR)/bin/$(PYTHON_BASENAME)
PIP := $(VENV_DIR)/bin/pip
PIP_COMPILE := $(VENV_DIR)/bin/pip-compile
PIP_SYNC := $(VENV_DIR)/bin/pip-sync

# Détermine l'environnement à utiliser.
ifndef ENV
  ifdef DJANGO_SETTINGS_MODULE
    ENV = $(shell echo $(DJANGO_SETTINGS_MODULE) | cut -d. -f3)
  else
    DEFAULT_ENV := production
    ENV = $(shell \
      sed -n '/^ENV/s/[^=]*=\(.*\)/\1/p' config.env 2> /dev/null \
        | tail -n 1 | grep -Ee '^..*' || echo "$(DEFAULT_ENV)")
  endif
endif

# Définis EDITOR pour l'édition interactive.
ifndef EDITOR
  ifdef VISUAL
    EDITOR := $(VISUAL)
  else
    EDITOR := vi
  endif
endif

# Définition des cibles -------------------------------------------------------
.DEFAULT_GOAL := help

# Commentaire d'une cible : #-> interne ##-> aide production+dev ###-> aide dev
.PHONY: help
help: ## affiche cette aide
ifeq ($(ENV), production)
	@perl -nle'print $& if m{^[a-zA-Z_-]+:[^#]*?## .*$$}' $(MAKEFILE_LIST) \
	  | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
else
	@perl -nle'print $& if m{^[a-zA-Z_-]+:[^#]*?###? .*$$}' $(MAKEFILE_LIST) \
	  | sort | awk 'BEGIN {FS = ":.*?###? "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
endif

.PHONY: clean
clean: clean-build clean-pyc clean-static ## nettoie tous les fichiers temporaires

.PHONY: clean-build
clean-build: ### nettoie les fichiers de construction du paquet
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info

.PHONY: clean-pyc
clean-pyc: ### nettoie les fichiers temporaires python
	find locolys/ \
	  \( -name '*.pyc' -o -name '*.pyo' -o -name '*~' \) -exec rm -f {} +

.PHONY: clean-static
clean-static: ### nettoie les fichiers "static" collectés
	rm -rf var/static

.PHONY: init
init: create-venv config.env ## initialise l'environnement et l'application
	@$(MAKE) --no-print-directory update

config.env:
	cp config.env.example config.env
	chmod go-rwx config.env
	$(EDITOR) config.env

.PHONY: update
update: check-config install-requirements migrate static ## mets à jour l'application et ses dépendances
	touch locolys/wsgi.py

.PHONY: check
check: check-config ## vérifie la configuration de l'instance
	$(PYTHON) manage.py check

.PHONY: check-config
check-config:
	@find . -maxdepth 1 -name config.env -perm /o+rwx -exec false {} + || \
	{ echo "\033[31mErreur :\033[0m les permissions de config.env ne sont pas bonnes, \
	vous devriez au moins faire : chmod o-rwx config.env"; false; }

.PHONY: install-requirements
install-requirements: install-piptools ## installe les dépendances de l'application
ifeq ($(ENV), production)
	$(PIP_SYNC) requirements.txt
else
	$(PIP_SYNC) dev-requirements.txt
endif

.PHONY: install-piptools
install-piptools:
	$(PIP) install --upgrade "pip-tools~=7.4.1"

.PHONY: migrate
migrate: ## mets à jour le schéma de la base de données
	$(PYTHON) manage.py migrate

.PHONY: static
static: ## collecte les fichiers statiques
ifeq ($(ENV), production)
	@echo "Collecte des fichiers statiques..."
	$(PYTHON) manage.py collectstatic --no-input --verbosity 0
endif

## Cibles liées à l'environnement virtuel

.PHONY: create-venv
create-venv: $(PYTHON)

$(PYTHON):
	$(shell which $(PYTHON_BASENAME)) -m venv $(VENV_OPT) $(VENV_DIR)

.PHONY: clear-venv
clear-venv: ## supprime l'environnement virtuel
	-rm -rf $(VENV_DIR)

## Cibles pour le développement

.PHONY: serve
serve: ### démarre un serveur local pour l'application
	$(PYTHON) manage.py runserver

.PHONY: test
test: ### lance les tests de l'application
	$(PYTHON) -m pytest --cov --cov-report=term:skip-covered

.PHONY: test-wip
test-wip: #### lance les tests marqués 'wip'
	$(PYTHON) -m pytest -vv -m 'wip' --pdb

.PHONY: test-failed
test-failed: #### lance les tests qui ont échoué
	$(PYTHON) -m pytest --lf

.PHONY: coverage
coverage: test ### vérifie la couverture de code
	$(PYTHON) -m coverage html
	@echo open htmlcov/index.html

.PHONY: lint
lint: ### vérifie la syntaxe du code Python
	@$(PYTHON) -m ruff check
	@$(PYTHON) -m ruff format --check --quiet

.PHONY: format
format: ### formate le code Python
	$(PYTHON) -m ruff check --fix
	$(PYTHON) -m ruff format

.PHONY: requirements
requirements: install-piptools ### verrouille les dépendances de l'application
	$(PIP_COMPILE) --upgrade --output-file=requirements.txt pyproject.toml
	$(PIP_COMPILE) --extra=dev --upgrade --output-file=dev-requirements.txt pyproject.toml

.PHONY: shell
shell: ### lance un shell Python dans l'environnement
ifeq ($(ENV), production)
	$(PYTHON) manage.py shell
else
	$(PYTHON) manage.py shell_plus
endif
